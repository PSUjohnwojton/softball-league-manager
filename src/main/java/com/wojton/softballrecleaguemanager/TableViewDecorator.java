package com.wojton.softballrecleaguemanager;

import com.wojton.softballrecleaguemanager.dataobjects.Player;
import com.wojton.softballrecleaguemanager.dataobjects.Team;
import javafx.geometry.Insets;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class TableViewDecorator {
    private TableView table;
    private VBox vbox;
    private Team team;

    public TableViewDecorator() {

    }

    public TableView getTable() {
        return table;
    }

    public List<Field> getColumnNames(Class c)
    {
        return Arrays.asList(c.getDeclaredFields());
    }
    public void TableViewDefaultDecorator(VBox vbox, Class c) {
       this.table = new TableView();
        this.vbox = vbox;
        table.setEditable(true);

        getColumnNames(c).forEach(field ->
        {
            TableColumn<Player, String> column1 = new TableColumn<>(field.getName());
            column1.setCellValueFactory(new PropertyValueFactory<>(field.getName()));
            table.getColumns().add(column1);
            table.getSelectionModel().setCellSelectionEnabled(true);
            table.getEditingCell();
            table.setEditable(true);
        });

        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 0, 0, 10));
        vbox.getChildren().add (table);
    }
    public void TableCustomColDecorator(VBox vbox, Map<String, String> colVal) {
        this.table = new TableView();
        this.vbox = vbox;
        table.setEditable(true);
        colVal.forEach((key, val) ->{
            TableColumn<String, String> column1 = new TableColumn<>(key);
            column1.setCellValueFactory(new PropertyValueFactory<>(val));
            table.getColumns().add(column1);
            table.getSelectionModel().setCellSelectionEnabled(true);
        });


        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 0, 0, 10));
        vbox.getChildren().add (table);
    }
   public void populateTable(List<?> list)
    {
        this.table.getItems().clear();
        this.table.getItems().addAll(list);
    }
    void setTeam(Team team)
    {
        this.team = team;
    }

    public VBox getVbox() {
        return vbox;
    }

    public void setVbox(VBox vbox) {
        this.vbox = vbox;
    }
}
