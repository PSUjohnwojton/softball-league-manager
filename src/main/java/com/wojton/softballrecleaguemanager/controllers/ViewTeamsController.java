package com.wojton.softballrecleaguemanager.controllers;

import com.wojton.softballrecleaguemanager.SoftballRecLeagueManager;
import com.wojton.softballrecleaguemanager.dataobjects.Team;
import com.wojton.softballrecleaguemanager.models.TeamModel;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.HashMap;

public class ViewTeamsController {
    @FXML
    VBox vBox;
    @FXML
    Button backButton;

    @FXML
    public void initialize()
    {
        backButton.setText("back");
        ListView listView = new ListView();
        HashMap<String, Team> teams =  new TeamModel().loadTeams();
        teams.forEach((teamName, team) -> {
            GridPane view = new GridPane();
            Label label = new Label();
            label.setText(teamName);
            Button button = new Button();
            button.setText("add player");
            button.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(ActionEvent e) {
                    Stage stage = (Stage) vBox.getScene().getWindow();
                    FXMLLoader fxmlLoader
                            = new FXMLLoader(SoftballRecLeagueManager.class.getResource("controllers/AddPlayer-View.fxml"));
                    try {
                        stage.getScene().setRoot(fxmlLoader.load());
                        AddPlayerController addPlayerController = (AddPlayerController) fxmlLoader.getController();
                        addPlayerController.setTeam(team);

                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            });

            Button viewButton = new Button();
            viewButton.setText("view players");
            viewButton.setOnAction(e -> {
                Stage stage = (Stage) vBox.getScene().getWindow();
                FXMLLoader fxmlLoader
                        = new FXMLLoader(SoftballRecLeagueManager.class.getResource("controllers/ViewPlayers-View.fxml"));
                try {
                    stage.getScene().setRoot(fxmlLoader.load());
                    PlayListViewController addPlayerController = (PlayListViewController) fxmlLoader.getController();
                    addPlayerController.setTeam(team);

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            });




            view.add(label, 0,0);
            view.add(button, 0, 1);
            view.add(viewButton, 0, 2);

            listView.getItems().add(view);
        });
        vBox.getChildren().add(listView);
    }
    public void onBackButton() throws IOException {
        Stage stage = (Stage) vBox.getScene().getWindow();
        FXMLLoader fxmlLoader = new FXMLLoader(SoftballRecLeagueManager.class.getResource("controllers/LeagueManager-view.fxml"));
        stage.getScene().setRoot(fxmlLoader.load());
    }
    public void onAddPlayerButton()
    {

    }
}
