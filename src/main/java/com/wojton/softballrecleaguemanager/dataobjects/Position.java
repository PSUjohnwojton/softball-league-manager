package com.wojton.softballrecleaguemanager.dataobjects;

public enum Position {
    PITCHER,
    CATCHER,
    FIRSTBASE,
    SECONDBASE,
    SHORTSTOP,
    THIRDBASE,
    LIFTFIELD,
    CENTERFIELD,
    RIGHTFIELD

}
