package com.wojton.softballrecleaguemanager.dataobjects;

public class WinsLoseTie {
    private int wins;
    private int loses;
    private int ties;
    private String teamName;
    public WinsLoseTie(String name)
    {
        this.teamName = name;
    }
    public WinsLoseTie(String name, int wins, int loses, int ties)
    {
        this.teamName = name;
        this.wins = wins;
        this.loses = loses;
        this.ties = ties;
    }

    public void addWin()
    {
        this.wins++;
    }
    public void addLoss()
    {
        this.loses++;
    }

    public void addTies()
    {
        this.ties++;
    }
    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getLoses() {
        return loses;
    }

    public void setLoses(int loses) {
        this.loses = loses;
    }

    public int getTies() {
        return ties;
    }

    public void setTies(int ties) {
        this.ties = ties;
    }

    @Override
    public boolean equals(Object object)
    {
        boolean isEquals = false;
        WinsLoseTie wlt;
        if(object instanceof WinsLoseTie)
        {
            wlt = (WinsLoseTie) object;
        }
        else
        {
            return isEquals;
        }
        if(this.getLoses() == wlt.getLoses()
        &&this.getWins() == wlt.getWins()
        &&this.getTies() == wlt.getTies())
        {
            isEquals = true;
        }
        return isEquals;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }
}
