package com.wojton.softballrecleaguemanager.controllers;

import com.wojton.softballrecleaguemanager.SoftballRecLeagueManager;
import com.wojton.softballrecleaguemanager.TableViewDecorator;
import com.wojton.softballrecleaguemanager.dataobjects.Player;
import com.wojton.softballrecleaguemanager.dataobjects.Team;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;

public class PlayListViewController {
    @FXML
    private VBox vBox;
    @FXML
    private Button backButton;
    private TableViewDecorator decorator;

    @FXML
    public void initialize() {
        backButton.setText("back");

    }
    public void setTeam(Team team)
    {
        decorator = new TableViewDecorator();
        decorator.TableViewDefaultDecorator(vBox, Player.class);
        decorator.populateTable(team.getAvailablePlayers());

    }
    public void onBackButton() throws IOException {
        Stage stage = (Stage) vBox.getScene().getWindow();
        FXMLLoader fxmlLoader = new FXMLLoader(SoftballRecLeagueManager.class.getResource("controllers/LeagueManager-View.fxml"));
        stage.getScene().setRoot(fxmlLoader.load());
    }
    public TableViewDecorator getDecorator()
    {
        return decorator;
    }
}
