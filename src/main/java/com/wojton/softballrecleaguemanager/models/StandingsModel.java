package com.wojton.softballrecleaguemanager.models;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wojton.softballrecleaguemanager.dataobjects.Standings;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class StandingsModel extends Model{
    static final private String userDataPath = System.getenv("LOCALAPPDATA") + "/data/userData";
    static final private String userDateFile = userDataPath + "/" + "standings.txt";

    static final private Gson gson = new GsonBuilder().setPrettyPrinting().create();;
    public StandingsModel()
    {

    }
    public static void saveStandings(Standings standings) throws IOException {
        checkFile(userDataPath, userDateFile);
        Standings standings1 = loadStandings();
        if (standings1 != null) {
            File scheduleFile = new File(userDateFile);
            scheduleFile.delete();
        }

        try (FileWriter writer = new FileWriter(userDateFile, false)) {
            try {
                writer.write(gson.toJson(standings));
                writer.flush();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public static Standings loadStandings()
    {
        return gson.fromJson(readFile(userDateFile), Standings.class);
    }
    public static String getUserDateFile()
    {
        return userDateFile;
    }
}
