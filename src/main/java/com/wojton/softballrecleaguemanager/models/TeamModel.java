package com.wojton.softballrecleaguemanager.models;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.wojton.softballrecleaguemanager.dataobjects.Team;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;

public class TeamModel extends Model {
   static final private String userDataPath = System.getenv("LOCALAPPDATA") + "/data/userData";
   static final private String userDateFile = userDataPath + "/" + "teams.txt";
   static final private Gson gson = new GsonBuilder().setPrettyPrinting().create();;
   static final private Type TeamListType = new TypeToken<HashMap<String,Team>>(){}.getType();
    public void TeamModel()
    {


    }
    public static boolean saveTeam(Team team) throws IOException {
        boolean saved = false;
        File credentials = new File(userDateFile);
        checkFile(userDataPath,userDateFile);
        HashMap<String, Team> teams = loadTeams();

        if(teams == null)
        {
            teams = new HashMap<>();
        }
        if(teams.containsKey(team.getName()))
        {
            teams.replace(team.getName(), team);
        }
        else
        {
            teams.put(team.getName(), team);
        }

            credentials.delete();
            try (FileWriter writer = new FileWriter(userDateFile, true)) {
                writer.append(gson.toJson(teams, TeamListType));
                writer.flush();
                writer.close();
                saved = true;
            }
        return saved;
    }
    public static  HashMap<String,Team> loadTeams() {
        HashMap<String,Team> teams = gson.fromJson(readFile(userDateFile), TeamListType);
        return  teams;
    }
}
