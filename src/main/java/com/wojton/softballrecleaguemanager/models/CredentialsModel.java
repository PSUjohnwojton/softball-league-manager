package com.wojton.softballrecleaguemanager.models;



import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.Scanner;




public class CredentialsModel {

   final private String userDataPath = System.getenv("LOCALAPPDATA") + "/data/userData";
   final private String userDateFile = userDataPath + "/" + "credentials.txt";
    public boolean saveCredentials(String userName, String password, String accountType) throws IOException {

        boolean saved = false;

        File dir = new File(userDataPath);
        File credentials = new File(userDateFile);
        if(!dir.exists()) {
          dir.mkdirs();
        }
        try {
            if (!credentials.exists() && !credentials.createNewFile()) {

                throw new NoSuchFileException(credentials.getAbsolutePath(), null, "File Can not be created");

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        if(!checkUsernameExists(userName)) {

            try (FileWriter writer = new FileWriter(userDateFile, true)) {
                writer.append(userName + "," + password + "," + accountType + "\n");
                writer.flush();
                saved = true;
            }
        }
        return saved;
    }

    public boolean loadCredentials(String userName, String password)  {

        ArrayList<String> data = readCredentialsFile();
        if(data == null)
        {
            return false;
        }
        return    data.stream().parallel().anyMatch(string->
            string.split(",")[0].equals(userName) &&
            string.split(",")[1].equals(password));
    }

    private boolean checkUsernameExists(String username)
    {
        return    readCredentialsFile().stream().parallel().anyMatch(string->
                string.split(",")[0].equals(username));
    }
    private ArrayList<String> readCredentialsFile()
    {
        File myObj = new File(userDateFile);
        ArrayList<String> data = new ArrayList<>();
        try (Scanner myReader = new Scanner(myObj)) {

            while (myReader.hasNextLine()) {

                data.add(myReader.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return data;
    }

}
