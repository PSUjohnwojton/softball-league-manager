package com.wojton.softballrecleaguemanager.seasoncreators;

import com.wojton.softballrecleaguemanager.dataobjects.Game;
import com.wojton.softballrecleaguemanager.dataobjects.Schedule;
import com.wojton.softballrecleaguemanager.dataobjects.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class regularSeasonCreatorTest extends regularSeasonCreator{

    @Test
    void testFindGameReturnNull()
    {
        Schedule schedule = new Schedule();
        Assertions.assertNull(schedule.findGame("1", "2"));
    }
    @Test
    void testFindGameReturnReturnsGame() {
        ArrayList<Team> teams = new ArrayList<Team>();
        for(int i = 0; i < 12; i++)
        {
            Team team = new Team();
            team.setName(Integer.toString(i));
            team.setOwner(Integer.toString(i));
            team.setMascot(Integer.toString(i));
            teams.add(team);
        }
        Schedule schedule  = this.createSchedule(teams);
        Assertions.assertNotNull(schedule.findGame("1", "2"));
    }
      @Test
    void createSchedule() {
        ArrayList<Team> teams = new ArrayList<Team>();
        for(int i = 0; i < 12; i++)
        {
            Team team = new Team();
            team.setName(Integer.toString(i));
            team.setOwner(Integer.toString(i));
            team.setMascot(Integer.toString(i));
            teams.add(team);
        }
        Schedule schedule  = this.createSchedule(teams);
        Assertions.assertTrue(schedule.getSchedule().size() == ((teams.size()/2)*(teams.size()-1)));

    }
    @Test
    void createSchedule4Team() {
        ArrayList<Team> teams = new ArrayList<Team>();
        for(int i = 0; i < 4; i++)
        {
            Team team = new Team();
            team.setName(Integer.toString(i));
            team.setOwner(Integer.toString(i));
            team.setMascot(Integer.toString(i));
            teams.add(team);
        }
        Schedule schedule  = this.createSchedule(teams);
        Assertions.assertTrue(schedule.getSchedule().size() == ((teams.size()/2)*(teams.size()-1)));

    }

    @Test
    void createSchedule8Team() {
        ArrayList<Team> teams = new ArrayList<Team>();
        for(int i = 0; i < 8; i++)
        {
            Team team = new Team();
            team.setName(Integer.toString(i));
            team.setOwner(Integer.toString(i));
            team.setMascot(Integer.toString(i));
            teams.add(team);
        }

        Schedule schedule  = this.createSchedule(teams);
        Assertions.assertTrue(schedule.getSchedule().size() == ((teams.size()/2)*(teams.size()-1)));
    }

    @Test
    void test4PrepWeeks()
    {
        ArrayList<Team> teams = new ArrayList<Team>();
        for(int i = 0; i < 4; i++)
        {
            Team team = new Team();
            team.setName(Integer.toString(i));
            team.setOwner(Integer.toString(i));
            team.setMascot(Integer.toString(i));
            teams.add(team);
        }
        prepWeeksSizeTest(teams);
        prepWeekNumberTest(teams);
    }
    @Test
    void test8PrepWeeks()
    {
        ArrayList<Team> teams = new ArrayList<Team>();
        for(int i = 0; i < 8; i++)
        {
            Team team = new Team();
            team.setName(Integer.toString(i));
            team.setOwner(Integer.toString(i));
            team.setMascot(Integer.toString(i));
            teams.add(team);
        }
        prepWeeksSizeTest(teams);
        prepWeekNumberTest(teams);
    }
    @Test
    void test12PrepWeeks()
    {
        ArrayList<Team> teams = new ArrayList<Team>();
        for(int i = 0; i < 8; i++)
        {
            Team team = new Team();
            team.setName(Integer.toString(i));
            team.setOwner(Integer.toString(i));
            team.setMascot(Integer.toString(i));
            teams.add(team);
        }
        prepWeeksSizeTest(teams);
        prepWeekNumberTest(teams);
    }
    void prepWeeksSizeTest(List<Team> teams)
    {
        this.prepWeeks(teams);

    }
    void prepWeekNumberTest(List<Team> teams) {
        for (Team team : teams) {
            Assertions.assertEquals(((teams.size() / 2) * (teams.size() - 1)), team.getWeeks().size());
            for (int i = 1; i <= team.getWeeks().size(); i++) {
                Assertions.assertTrue(team.getWeeks().stream().map(tea -> tea.getWeek()).collect(Collectors.toList()).contains(i));
            }
        }
    }
}