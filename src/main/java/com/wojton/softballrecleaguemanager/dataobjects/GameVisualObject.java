package com.wojton.softballrecleaguemanager.dataobjects;

public class GameVisualObject {
    protected String awayTeam;
    protected String homeTeam;
    protected int weekNumber;
    public GameVisualObject(String awayTeam, String homeTeam, int weekNumber)
    {
        this.awayTeam = awayTeam;
        this.homeTeam = homeTeam;
        this.weekNumber = weekNumber;
    }
    public GameVisualObject()
    {

    }
   public void setAwayTeam(String awayTeam)
    {
        this.awayTeam = awayTeam;
    }
    public void setHomeTeam(String homeTeam)
    {
        this.homeTeam = homeTeam;
    }
    public String getHomeTeam(){
        return this.homeTeam;
    }
    public String getAwayTeam()
    {
        return  this.awayTeam;
    }
    public int getWeekNumber()
    {
        return this.weekNumber;
    }
    public void setWeekNumber(int weekNumber)
    {
        this.weekNumber = weekNumber;
    }
}
