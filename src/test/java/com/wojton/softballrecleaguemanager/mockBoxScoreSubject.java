package com.wojton.softballrecleaguemanager;

import com.wojton.softballrecleaguemanager.dataobjects.BoxScore;
import com.wojton.softballrecleaguemanager.dataobjects.Inning;
import com.wojton.softballrecleaguemanager.dataobjects.Team;
import com.wojton.softballrecleaguemanager.observers.BoxScoreObserver;
import com.wojton.softballrecleaguemanager.subjects.boxScoreSubject;

public class mockBoxScoreSubject extends boxScoreSubject {
    private BoxScore boxScore = new BoxScore(new Team(), new Team());
    public mockBoxScoreSubject()
    {
        for(int i = 1; i<=9; i++)
        {
            Inning inning = new Inning(i);
            inning.setAwayRuns(i);
            inning.setAwayHits(i);
            inning.setAwayErrors(i);
            inning.setHomeErrors(i);
            inning.setHomeHits(i);
            inning.setHomeRuns(i);
            boxScore.getBoxScore().put(Integer.toString(i),inning);
        }

    }

    public BoxScore getBoxScore() {
        this.execute(boxScore);
        return boxScore;
    }

    public void addObserver(BoxScoreObserver observer)
    {
        super.addObeserver(observer);
    }
    public void removeObserver(BoxScoreObserver observer)
    {
        super.removeObserver(observer);
    }

    @Override
    protected void execute(BoxScore boxScore) {
        super.execute(boxScore);
    }
}
