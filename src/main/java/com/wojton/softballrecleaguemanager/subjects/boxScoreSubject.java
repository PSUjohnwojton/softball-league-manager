package com.wojton.softballrecleaguemanager.subjects;

import com.wojton.softballrecleaguemanager.dataobjects.BoxScore;
import com.wojton.softballrecleaguemanager.observers.BoxScoreObserver;

import java.util.ArrayList;

public abstract class boxScoreSubject {
    ArrayList<BoxScoreObserver> observers = new ArrayList<>();
    protected void addObeserver(BoxScoreObserver observer)
    {
        this.observers.add(observer);
    }
    protected void removeObserver(BoxScoreObserver observer)
    {
        this.observers.remove(observer);
    }
    protected void execute(BoxScore boxScore)
    {
        observers.stream().forEach(observer -> observer.notify(boxScore));
    }
}
