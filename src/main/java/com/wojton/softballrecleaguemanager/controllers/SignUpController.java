package com.wojton.softballrecleaguemanager.controllers;

import com.wojton.softballrecleaguemanager.SoftballRecLeagueManager;
import com.wojton.softballrecleaguemanager.models.CredentialsModel;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

public class SignUpController extends BorderPane{

    @FXML
    Label userName;
    @FXML
    Label password;
    @FXML
    Label confirmed;
    @FXML
    TextField userNameTextField;
    @FXML
    TextField passwordTextField;
    @FXML
    Button confirm;
    @FXML
    Button cancel;
    @FXML
    Button back;
    @FXML
    ComboBox accountType;
    private CredentialsModel credentialsModel;


    @FXML
    public void initialize() {
        userName.setText("User Name: ");
        password.setText("Password: ");
        confirm.setText("Confirm");
        cancel.setText("Cancel");
        back.setText("Back");
        accountType.getItems().addAll(
                "league manager",
                "owner",
                "regular user"
        );
        accountType.setValue("league manager");
        credentialsModel = new CredentialsModel();


       // BorderPane border = new BorderPane();
    //    Stage stage = (Stage) userName.getScene().getWindow();
        // these two of them return the same stage
        // Swap screen

//     t s
      //  Scene scene = new Scene(border, 320, 240);
     //   stage.setScene(scene);

    }
    @FXML
    protected void onConfirmClick() throws IOException {
        String userName = userNameTextField.getText();
        String passWord = passwordTextField.getText();
        try {
            credentialsModel.saveCredentials(userName, passWord, accountType.getValue().toString());
            confirmed.setText("Sign Up successful");
        }
        catch(IOException ex)
        {
            confirmed.setText("Error on Sign Up... Please Try again.");
        }

    }
    @FXML
    protected void onCancelClick() throws IOException {

    }
    @FXML
    protected void onBackClick() throws IOException{
        Stage stage = (Stage) userName.getScene().getWindow();
        // these two of them return the same stage
        // Swap screen

        FXMLLoader fxmlLoader = new FXMLLoader(SoftballRecLeagueManager.class.getResource("StartScreen-view.fxml"));
        stage.getScene().setRoot(fxmlLoader.load());
//        Scene scene = new Scene(fxmlLoader.load(), 320, 240);
//        stage.setScene(scene);
    }

}
