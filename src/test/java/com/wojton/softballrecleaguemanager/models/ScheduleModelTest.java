package com.wojton.softballrecleaguemanager.models;

import com.wojton.softballrecleaguemanager.dataobjects.BoxScore;
import com.wojton.softballrecleaguemanager.dataobjects.Schedule;
import com.wojton.softballrecleaguemanager.dataobjects.Standings;
import com.wojton.softballrecleaguemanager.dataobjects.Team;
import com.wojton.softballrecleaguemanager.mockBoxScoreSubject;
import com.wojton.softballrecleaguemanager.seasoncreators.regularSeasonCreator;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.Thread.sleep;

public class ScheduleModelTest extends RegularSeasonModel{
    static final private String userDataPath = System.getenv("LOCALAPPDATA") + "/data/userData";
    static final private String userDateFile = userDataPath + "/" + "Schedules.txt";
    static private String playoffs = Integer.toString(1) + "PlayoffSchedule.json";
    static final private String playoffFile = userDataPath + "/" + playoffs;
    @BeforeEach
    void setUp() {
        File file1 = new File(userDataPath);
        Arrays.stream(file1.list()).filter(name -> name.contains("PlayoffSchedule")).forEach(name->
        {
            new File(userDataPath + "/" + name).delete();
        });
        File myObj = new File(userDateFile);
        myObj.delete();
        myObj = new File(playoffFile);
        myObj.delete();
    }
    @Test
    public void saveScheduleToFileTest() throws IOException {
        ArrayList<Team> teams = new ArrayList<>();
        for(int i = 0; i<12; i++)
        {
            Team team = new Team();
            team.setName(Integer.toString(i));
            team.setOwner(Integer.toString(i));
            team.setMascot(Integer.toString(i));
            teams.add(team);

        }
        regularSeasonCreator creator = new regularSeasonCreator();

        creator.createSchedule(teams);
        Assertions.assertNotEquals("", RegularSeasonModel.readFile(RegularSeasonModel.getUserDateFile()));
    }
    @Test
    public void loadScheduleToFileTest() throws IOException, InterruptedException {
        ArrayList<Team> teams = new ArrayList<>();
        for(int i = 0; i<4; i++)
        {
            Team team = new Team();
            team.setName(Integer.toString(i));
            team.setOwner(Integer.toString(i));
            team.setMascot(Integer.toString(i));
            teams.add(team);

        }
        regularSeasonCreator creator = new regularSeasonCreator();
        Schedule schedule = creator.createSchedule(teams);
        RegularSeasonModel.saveSchedule(schedule);
            Assertions.assertEquals(schedule.getSchedule().size(), RegularSeasonModel.loadSchedule().getSchedule().size());
    }
    @Test
    public void getScheduleByMakingIt()  {
        ArrayList<Team> teams = new ArrayList<>();
        for(int i = 0; i<4; i++)
        {
            Team team = new Team();
            team.setName(Integer.toString(i));
            team.setOwner(Integer.toString(i));
            team.setMascot(Integer.toString(i));
            teams.add(team);

        }
        regularSeasonCreator creator = new regularSeasonCreator();
        RegularSeasonModel.setUserDateFile(false);
        Schedule schedule = RegularSeasonModel.getSchedule(teams);

        Assertions.assertNotEquals(0, schedule.getSchedule().size());

    }
    @Test
    public void getScheduleByReadingFromFile() throws IOException {
        ArrayList<Team> teams = new ArrayList<>();
        Map<String, Team> teamsMap = new HashMap<>();
        for(int i = 0; i<4; i++)
        {
            Team team = new Team();
            team.setName(Integer.toString(i));
            team.setOwner(Integer.toString(i));
            team.setMascot(Integer.toString(i));
            teams.add(team);
            teamsMap.put(team.getName(),team);

        }
        Standings standings = new Standings(teamsMap);
        StandingsModel.saveStandings(standings);
        regularSeasonCreator creator = new regularSeasonCreator();
        creator.createSchedule(teams);
        Schedule schedule = RegularSeasonModel.getSchedule(teams);

        Assertions.assertNotEquals(0, schedule.getSchedule().size());

    }
    @Test public void testBoxScoreSave() throws IOException {
        ArrayList<Team> teams = new ArrayList<>();
        for(int i = 0; i<4; i++)
        {
            Team team = new Team();
            team.setName(Integer.toString(i));
            team.setOwner(Integer.toString(i));
            team.setMascot(Integer.toString(i));
            teams.add(team);

        }
        regularSeasonCreator creator = new regularSeasonCreator();
        creator.createSchedule(teams);
        Schedule schedule = RegularSeasonModel.getSchedule(teams);
        mockBoxScoreSubject boxScoreSubject = new mockBoxScoreSubject();

        schedule.getSchedule().get(0).setBoxScore(boxScoreSubject.getBoxScore());
        RegularSeasonModel.saveSchedule(schedule);
        schedule = RegularSeasonModel.loadSchedule();
        boxScoreSubject.getBoxScore().getBoxScore().get(String.valueOf(1)).getAwayHits();
        boxScoreSubject.getBoxScore().getBoxScore().get(String.valueOf(1)).getAwayRuns();
        boxScoreSubject.getBoxScore().getBoxScore().get(String.valueOf(1)).getHomeErrors();
        boxScoreSubject.getBoxScore().getBoxScore().get(String.valueOf(1)).getHomeHits();
        boxScoreSubject.getBoxScore().getBoxScore().get(String.valueOf(1)).getHomeRuns();
       Assertions.assertEquals(  boxScoreSubject.getBoxScore().getBoxScore().get(String.valueOf(1)).getAwayErrors(),
               schedule.getSchedule().get(0).getBoxScore().getBoxScore().get(String.valueOf(1)).getAwayErrors());
        Assertions.assertEquals(  boxScoreSubject.getBoxScore().getBoxScore().get(String.valueOf(1)).getAwayHits(),
                schedule.getSchedule().get(0).getBoxScore().getBoxScore().get(String.valueOf(1)).getAwayHits());
        Assertions.assertEquals(  boxScoreSubject.getBoxScore().getBoxScore().get(String.valueOf(1)).getAwayRuns(),
                schedule.getSchedule().get(0).getBoxScore().getBoxScore().get(String.valueOf(1)).getAwayRuns());
        Assertions.assertEquals(  boxScoreSubject.getBoxScore().getBoxScore().get(String.valueOf(1)).getHomeErrors(),
                schedule.getSchedule().get(0).getBoxScore().getBoxScore().get(String.valueOf(1)).getHomeErrors());
        Assertions.assertEquals(  boxScoreSubject.getBoxScore().getBoxScore().get(String.valueOf(1)).getHomeHits(),
                schedule.getSchedule().get(0).getBoxScore().getBoxScore().get(String.valueOf(1)).getHomeHits());
        Assertions.assertEquals(  boxScoreSubject.getBoxScore().getBoxScore().get(String.valueOf(1)).getHomeRuns(),
                schedule.getSchedule().get(0).getBoxScore().getBoxScore().get(String.valueOf(1)).getHomeRuns());
    }
    @Test
    void getRegSeasonFileTest()
    {
        ArrayList<Team> teams = new ArrayList<>();
        for(int i = 0; i<4; i++)
        {
            Team team = new Team();
            team.setName(Integer.toString(i));
            team.setOwner(Integer.toString(i));
            team.setMascot(Integer.toString(i));
            teams.add(team);

        }
        RegularSeasonModel.setUserDateFile(false);

        Assertions.assertNotNull( RegularSeasonModel.getSchedule(teams));
    }
    @Test
    void getPlayoffSeasonFileTest()
    {
        ArrayList<Team> teams = new ArrayList<>();
        for(int i = 0; i<4; i++)
        {
            Team team = new Team();
            team.setName(Integer.toString(i));
            team.setOwner(Integer.toString(i));
            team.setMascot(Integer.toString(i));
            teams.add(team);

        }
        RegularSeasonModel.setUserDateFile(true);
        RegularSeasonModel.setRoundNumber(1);
        Assertions.assertNotNull( RegularSeasonModel.getSchedule(teams));
    }
    @Test
    void setTotalRoundTests()
    {
        RegularSeasonModel.setRoundNumber(2);
        Assertions.assertEquals(2, RegularSeasonModel.getRound());
    }
    @Test
    void getPlayoffNumTest() throws IOException {
        File file = new File(playoffFile);
        file.createNewFile();
        Assertions.assertEquals(1,RegularSeasonModel.getPlayOffFilNum());
    }
}
