package com.wojton.softballrecleaguemanager.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wojton.softballrecleaguemanager.SoftballRecLeagueManager;
import com.wojton.softballrecleaguemanager.TableViewDecorator;
import com.wojton.softballrecleaguemanager.dataobjects.*;
import com.wojton.softballrecleaguemanager.models.RegularSeasonModel;
import com.wojton.softballrecleaguemanager.models.StandingsModel;
import com.wojton.softballrecleaguemanager.observers.BoxScoreObserver;
import com.wojton.softballrecleaguemanager.seasoncreators.PlayOffSeasonCreator;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class ViewPlayoffScheduleController {
    @FXML
    private VBox vBox;
    @FXML
    Button backButton;
    @FXML
    Button viewGameButton;
    @FXML
    ComboBox round;
    private Schedule firstRoundPlayoffSchedule;
//    private int totalRounds = 1;
    private TableViewDecorator decorator;
    private GameViewController controller;
    private List<BoxScoreObserver> observers = new ArrayList<>();
    private List<Set<Team>> listOfTeams = new ArrayList<>();
    @FXML
    public void initialize() {

        ArrayList<Inning> lists = new ArrayList<>();
//        RegularSeasonModel.getSchedule(new ArrayList<>(TeamModel.loadTeams().values())).getSchedule()
//                .stream().map(game -> game.getBoxScore().getBoxScore().values()).forEach(list ->
//        {
//            lists.addAll(list);
//        });

//        if(lists.stream().allMatch(inning -> inning.isComplete() == true))
//        {

       // }
//        if(RegularSeasonModel.getPlayOffFilNum() == 1)
//        {

        round.getItems().clear();
        round.getItems().add("1");
        for (int i = 1; i <= RegularSeasonModel.getPlayOffFilNum(); i++) {
            System.out.println("beginning: " + i);
            boolean roundComplete = false;
         //   round.getItems().add(Integer.toString(1));

            RegularSeasonModel.setRoundNumber(i);
            RegularSeasonModel.setUserDateFile(true);
            System.out.println(RegularSeasonModel.getUserDateFile());
            Schedule schedule = RegularSeasonModel.loadSchedule();
            if(schedule != null) {
                List<Collection<Inning>> innings =
                        schedule.getSchedule()
                                .stream().map(game -> game.getBoxScore().getBoxScore().values()).collect(Collectors.toList());
                for (Collection<Inning> list : innings) {

                    System.out.println("round complete");
                    roundComplete = list.stream().allMatch(inning -> inning.isComplete());

                }
                if (roundComplete) {
                    schedule.getSchedule().stream().map(game -> game.getBoxScore()).forEach(boxScore ->
                    {
                        HashSet<Team> playoffTeams = new HashSet<>();
                        boxScore.sumHits();
                        if(boxScore.getHomeScore() < boxScore.getAwayScore())
                        {
                            playoffTeams.add(boxScore.getAwayTeam());
                        }
                        else
                        {
                            playoffTeams.add(boxScore.getHomeTeam());
                        }
                        listOfTeams.add(playoffTeams);
                    });
                    System.out.println("this is i: " + i);
                    round.getItems().add(Integer.toString(i + 1));
                }
            }
        }
//        else {
//            for (int i = 1; i <= RegularSeasonModel.getPlayOffFilNum(); i++) {
//                round.getItems().add(Integer.toString(i));
//                //   RegularSeasonModel.setRoundNumber(i);
//            }
//        }
        round.setValue("1");
        backButton.setText("back");
        viewGameButton.setText("View Selected Game");
        RegularSeasonModel.setRoundNumber(1);
        RegularSeasonModel.setUserDateFile(true);

    }
    public void setSchedule(Schedule schedule)
    {
        firstRoundPlayoffSchedule = schedule;
        HashMap<String, String> map = new HashMap<>();
        map.put("Away Team", "awayTeam");
        map.put("Home Team", "homeTeam");
        map.put("Week", "weekNumber");
        if(decorator == null )
        {
           decorator =  new TableViewDecorator();
           decorator.TableCustomColDecorator(vBox, map);
        }

        ArrayList<GameVisualObject> gameVisualObjectArrayList = new ArrayList<>();

        schedule.getSchedule().stream().forEach(game->{
            gameVisualObjectArrayList.add(new GameVisualObject(game.getAwayTeam().getName(), game.getHomeTeam().getName(), game.getWeekNumber()));
        });
        decorator.populateTable(gameVisualObjectArrayList);

    }
    @FXML
    void onBackButton() throws IOException {
        Stage stage = (Stage) vBox.getScene().getWindow();
        FXMLLoader fxmlLoader = new FXMLLoader(SoftballRecLeagueManager.class.getResource("controllers/LeagueManager-View.fxml"));
        stage.getScene().setRoot(fxmlLoader.load());


    }
    @FXML
    void onViewGameButton() throws IOException {

        GameVisualObject game = (GameVisualObject)  decorator.getTable().getSelectionModel().getSelectedItem();
        Stage stage = (Stage) vBox.getScene().getWindow();
        FXMLLoader fxmlLoader = new FXMLLoader(SoftballRecLeagueManager.class.getResource("controllers/Game-View.fxml"));

        stage.getScene().setRoot(fxmlLoader.load());

        this.controller = (GameViewController)fxmlLoader.getController();
        this.controller.setRound(Integer.valueOf(this.round.getValue().toString()));
        this.controller.setPlayoff(true);
        this.controller.addText(game);

    }
    public void addObserver(BoxScoreObserver observer)
    {
        this.observers.add(observer);
        //  this.controller.getController().addObserver(observer);
    }
    public void onRound() throws IOException {


        int value = Integer.valueOf((String) this.round.getValue());


          RegularSeasonModel.setRoundNumber(value);
        PlayOffSeasonCreator creator = new PlayOffSeasonCreator(StandingsModel.loadStandings());
        ArrayList<Team> teams = new ArrayList<>();
        RegularSeasonModel.setUserDateFile(true);
       if( RegularSeasonModel.loadSchedule() != null)
       {
           this.setSchedule(RegularSeasonModel.loadSchedule());
           return;
       }

        if(value!=1)
        {
            RegularSeasonModel.setRoundNumber(value-1);

        }
        else
        {
            RegularSeasonModel.setRoundNumber(value);
        }
        RegularSeasonModel.setUserDateFile(true);

        RegularSeasonModel.getSchedule(/*new ArrayList<>(TeamModel.loadTeams().values())*/null).getSchedule().stream().map(game -> game.getBoxScore()).forEach(boxScore ->
        {
            boxScore.sumHits();
            if(boxScore.getHomeScore() < boxScore.getAwayScore())
            {
                teams.add(boxScore.getAwayTeam());
            }
            else
            {
                teams.add(boxScore.getHomeTeam());
            }
        });
        System.out.println("Team size: " + teams.size());
        RegularSeasonModel.setRoundNumber(value);
        RegularSeasonModel.setUserDateFile(true);


        Schedule loadedSchedule = RegularSeasonModel.getSchedule(teams);
        RegularSeasonModel.saveSchedule(loadedSchedule);
        if(loadedSchedule == null || loadedSchedule.getSchedule().isEmpty()) {
            Schedule schedule = creator.createSchedule(teams);
            Gson gson = new GsonBuilder().setPrettyPrinting().create();

            System.out.println(gson.toJson(schedule));
            RegularSeasonModel.setRoundNumber(value);
            RegularSeasonModel.setUserDateFile(true);
            RegularSeasonModel.saveSchedule(schedule);
            this.setSchedule(schedule);
        }
        else
        {
            this.setSchedule(loadedSchedule);
        }

    }
    public TableViewDecorator getDecorator()
    {
        return decorator;
    }
}
