package com.wojton.softballrecleaguemanager.dataobjects;

public class Game {
    private Team homeTeam;
    private Team awayTeam;
    private int weekNumber;
    private BoxScore boxScore;
//    public Game(Team awayTeam, Team homeTeam)
//    {
//        this.awayTeam = awayTeam;
//        this.homeTeam = homeTeam;
//    }

    public void setWeekNumber(int weekNumber)
    {
        this.weekNumber = weekNumber;
    }
    public int getWeekNumber()
    {
        return this.weekNumber;
    }
    public Team getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    public Team getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(Team awayTeam) {
        this.awayTeam = awayTeam;
    }

    public BoxScore getBoxScore() {
        if(boxScore== null)
        {
            boxScore = new BoxScore(awayTeam, homeTeam);
        }
        return boxScore;
    }

    public void setBoxScore(BoxScore boxScore) {
        this.boxScore = boxScore;
    }
}
