package com.wojton.softballrecleaguemanager.controllers;

import com.wojton.softballrecleaguemanager.SoftballRecLeagueManager;
import com.wojton.softballrecleaguemanager.dataobjects.Schedule;
import com.wojton.softballrecleaguemanager.dataobjects.Standings;
import com.wojton.softballrecleaguemanager.dataobjects.Team;
import com.wojton.softballrecleaguemanager.models.RegularSeasonModel;
import com.wojton.softballrecleaguemanager.models.TeamModel;
import com.wojton.softballrecleaguemanager.observers.PlayoffBoxScoreObserver;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

public class LeagueManagerController {
    private PlayoffBoxScoreObserver playoffBoxScoreObserver = new PlayoffBoxScoreObserver();
    @FXML
    protected BorderPane borderPane;
    @FXML
    protected Button addTeam;
    @FXML
    protected Button viewTeams;
    @FXML
    protected Button createRegSchedule;
    @FXML
    protected Button viewStandings;
    @FXML
    protected Button createPlayoffSchedule;

    private static Standings standings;
    @FXML
    public void initialize() {
        createPlayoffSchedule.setText("View Playoff Schedule");
        viewStandings.setText("View Standings");
        createRegSchedule.setText("create schedule");
        addTeam.setText("Add New Team");
        viewTeams.setText("View Teams");
        ListView listView = new ListView();
        listView.getItems().add(addTeam);
        listView.getItems().add(viewTeams);
        listView.getItems().add(createRegSchedule);
        listView.getItems().add(viewStandings);
        listView.getItems().add(createPlayoffSchedule);
        borderPane.setLeft(listView);
        borderPane.setBorder(new Border(new BorderStroke(Color.BLACK,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));


        createRegSchedule.setOnAction(e -> {
            RegularSeasonModel.setUserDateFile(false);
            Stage stage = (Stage) borderPane.getScene().getWindow();
            FXMLLoader fxmlLoader
                    = new FXMLLoader(SoftballRecLeagueManager.class.getResource("controllers/ViewSchedule-View.fxml"));
            try {
                stage.getScene().setRoot(fxmlLoader.load());
                ViewScheduleController viewScheduleController = (ViewScheduleController) fxmlLoader.getController();
              ArrayList<Team> teams = new ArrayList<>( TeamModel.loadTeams().values());

                  viewScheduleController.setSchedule(RegularSeasonModel.getSchedule(teams));
                standings = new Standings(TeamModel.loadTeams());
                ;
                ViewScheduleController controller = fxmlLoader.getController();
                controller.addObserver(standings);
                RegularSeasonModel.setUserDateFile(false);
                standings.setStandings(RegularSeasonModel.getSchedule(teams));
                System.out.println("This is the end");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });


    }

    public void onAddTeamClick() throws IOException {

        Stage stage = (Stage) addTeam.getScene().getWindow();
        FXMLLoader fxmlLoader = new FXMLLoader(SoftballRecLeagueManager.class.getResource("controllers/AddTeam-View.fxml"));
       // stage.getScene().setRoot(fxmlLoader.load());
       // borderPane.getLeft().autosize();
      //  borderPane.setPadding(new Insets(10, 10, 10, 10));

        borderPane.setCenter(fxmlLoader.load());
       // borderPane.getCenter().setLayoutX(50);
     //   borderPane.setCenter();

    }
    public void onViewTeamClick() throws IOException{
        Stage stage = (Stage) addTeam.getScene().getWindow();
        FXMLLoader fxmlLoader = new FXMLLoader(SoftballRecLeagueManager.class.getResource("controllers/ViewTeam-View.fxml"));
        borderPane.setCenter(fxmlLoader.load());
        //stage.getScene().setRoot(fxmlLoader.load());
    }



    public void onCreateRegSchedule() throws IOException {
        RegularSeasonModel.setUserDateFile(false);
        System.out.println("this is the beginning");
        Stage stage = (Stage) addTeam.getScene().getWindow();
        FXMLLoader fxmlLoader = new FXMLLoader(SoftballRecLeagueManager.class.getResource("controllers/ViewSchedule-View.fxml"));
        borderPane.setCenter(fxmlLoader.load());
        standings = new Standings(TeamModel.loadTeams());
        ViewScheduleController controller = fxmlLoader.getController();
        controller.addObserver(standings);
        System.out.println("This is the end");
        RegularSeasonModel.setUserDateFile(false);
        standings.setStandings(RegularSeasonModel.loadSchedule());





    }


    public void onCreatePlayoffSchedule() throws IOException {

        Stage stage = (Stage) addTeam.getScene().getWindow();
        FXMLLoader fxmlLoader = new FXMLLoader(SoftballRecLeagueManager.class.getResource("controllers/ViewPlayoffSchedule-View.fxml"));
        borderPane.setCenter(fxmlLoader.load());
        ViewPlayoffScheduleController controller = fxmlLoader.getController();
        standings = new Standings(TeamModel.loadTeams());
        controller.addObserver(standings);
        ArrayList<Team> teams = new ArrayList<>( TeamModel.loadTeams().values());
        RegularSeasonModel.setRoundNumber(1);
        RegularSeasonModel.setUserDateFile(true);
        Schedule schedule = RegularSeasonModel.getSchedule(teams);
        controller.setSchedule(schedule);
        playoffBoxScoreObserver.setTeams(teams);
        playoffBoxScoreObserver.setSchedule(schedule);
        controller.addObserver(playoffBoxScoreObserver);

        RegularSeasonModel.saveSchedule(schedule);

    }

    public void onViewStandings() throws IOException {
        System.out.println("this is the beginning");
        Stage stage = (Stage) addTeam.getScene().getWindow();
        FXMLLoader fxmlLoader = new FXMLLoader(SoftballRecLeagueManager.class.getResource("controllers/ViewStandings-View.fxml"));
        borderPane.setCenter(fxmlLoader.load());

    }
}
