package com.wojton.softballrecleaguemanager.controllers;

import com.wojton.softballrecleaguemanager.dataobjects.BoxScore;
import com.wojton.softballrecleaguemanager.dataobjects.Inning;
import com.wojton.softballrecleaguemanager.dataobjects.Schedule;
import com.wojton.softballrecleaguemanager.models.RegularSeasonModel;
import com.wojton.softballrecleaguemanager.observers.BoxScoreObserver;
import com.wojton.softballrecleaguemanager.subjects.boxScoreSubject;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.util.ArrayList;

;

public class AddScoreController extends boxScoreSubject {

    @FXML
    Label awayRuns;
    @FXML
    TextField awayRunsField;
    @FXML
    Label awayErrors;
    @FXML
    TextField awayErrorsField;
    @FXML
    Label awayHits;
    @FXML
    TextField awayHitsField;

    @FXML
    Label homeRuns;
    @FXML
    TextField homeRunsField;
    @FXML
    Label homeErrors;
    @FXML
    TextField homeErrorsField;
    @FXML
    Label homeHits;
    @FXML
    TextField homeHitsField;
    @FXML
    Label inningField;
    @FXML
    ComboBox inningCombo;
    @FXML
    Button Save;

   private String homeTeam;
   private String awayTeam;
   private GameViewController parentController;
   private ArrayList<BoxScoreObserver> observers = new ArrayList<>();

    private Schedule schedule;// RegularSeasonModel.loadSchedule();


    @FXML
    public void initialize() {

        Save.setText("Save");
        this.awayHits.setText("Away Hits: ");
        this.awayErrors.setText("Away Errors: ");
        this.awayRuns.setText("Away Runs: ");
        this.homeErrors.setText("HomeErrors: ");
        this.homeHits.setText("Home Hits: ");
        this.homeRuns.setText("Home Runs: ");
        this.inningField.setText("Inning: ");
        for(int i = 1; i <= 9; i++)
        {
            this.inningCombo.getItems().add(i);
        }
        homeHitsField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    homeHitsField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        homeErrorsField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    homeErrorsField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        homeRunsField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    homeRunsField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        awayRunsField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    awayRunsField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        awayErrorsField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    awayErrorsField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        awayHitsField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    awayHitsField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
    }
    @FXML
    void onSaveGameBoxScore() throws IOException {
       // BoxScore boxScore = new BoxScore();
        Schedule schedule;
        if(!this.parentController.isPlayoff())
        {
            RegularSeasonModel.setUserDateFile(false);
            schedule = RegularSeasonModel.loadSchedule();
        }
        else
        {
            RegularSeasonModel.setUserDateFile(true);
            schedule = RegularSeasonModel.loadSchedule();
        }

        BoxScore boxScore =schedule.findGame(this.homeTeam, this.awayTeam).getBoxScore();

        Inning inning = new Inning(Integer.valueOf(inningCombo.getValue().toString()));
        inning.setAwayErrors(Integer.valueOf(this.awayErrorsField.getText()));
        inning.setAwayHits(Integer.valueOf(this.awayHitsField.getText()));
        inning.setAwayRuns(Integer.valueOf(this.awayRunsField.getText()));
        inning.setHomeErrors(Integer.valueOf(this.homeErrorsField.getText()));
        inning.setHomeHits(Integer.valueOf(this.homeHitsField.getText()));
        inning.setHomeRuns(Integer.valueOf(this.homeRunsField.getText()));
        inning.setComplete(true);
        boxScore.addInning(inning);
  //      if(!parentController.isPlayoff()) {
            schedule.findGame(this.homeTeam, this.awayTeam).setBoxScore(boxScore);

            RegularSeasonModel.saveSchedule(schedule);
//        }else{
//            schedule.findGame(this.homeTeam, this.awayTeam).setBoxScore(boxScore);
//            PlayoffSeasonModel.saveSchedule(schedule);
//        }

        this.execute(boxScore);

    }
    public void setParentController(GameViewController controller)
    {
        this.parentController = controller;
        this.addObeserver(controller);
    }
    public void addObserver(BoxScoreObserver observer)
    {
        super.addObeserver(observer);
    }
    public void setHomeTeam(String team)
    {
        this.homeTeam = team;
    }
    public void setAwayTeam(String team)
    {
        this.awayTeam = team;
    }
    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }
}
