package com.wojton.softballrecleaguemanager.observers;

import com.wojton.softballrecleaguemanager.dataobjects.BoxScore;
import com.wojton.softballrecleaguemanager.dataobjects.Schedule;
import com.wojton.softballrecleaguemanager.dataobjects.Standings;
import com.wojton.softballrecleaguemanager.dataobjects.Team;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PlayoffBoxScoreObserver implements BoxScoreObserver{
    private Schedule schedule;
    private ArrayList<Team> teams;
    private Standings standings;
    private Map<Integer, Schedule> rounds = new HashMap<>();
    @Override
    public void notify(BoxScore boxscore) {

    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public ArrayList<Team> getTeams() {
        return teams;
    }

    public void setTeams(ArrayList<Team> teams) {
        this.teams = teams;
        Map<String, Team> teamMap = new HashMap<>();
        teams.stream().forEach(team -> teamMap.put(team.getName(), team));
        this.standings = new Standings(teamMap);
    }
}
