package com.wojton.softballrecleaguemanager.controllers;

import com.wojton.softballrecleaguemanager.SoftballRecLeagueManager;
import com.wojton.softballrecleaguemanager.TableViewDecorator;
import com.wojton.softballrecleaguemanager.dataobjects.*;
import com.wojton.softballrecleaguemanager.models.RegularSeasonModel;
import com.wojton.softballrecleaguemanager.observers.BoxScoreObserver;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Arrays;

public class GameViewController implements BoxScoreObserver {

    private boolean isPlayoff = false;
    @FXML
    Label weekNumber;
    @FXML
    Label awayTeam;
    @FXML
    Label homeTeam;
    @FXML
    Label VS;
    @FXML
    Button back;
    @FXML
    Button addBoxScore;
    @FXML
    GridPane grid;
    private int round =1;
    private AddScoreController controller;
    private Game game;
    private BoxScore boxScore;
    private TableViewDecorator create = new TableViewDecorator();
    VBox vbox = new VBox();

    @FXML
    public void initialize() {
        this.grid.add(vbox, 3,5);
        this.addBoxScore.setText("Add Box Score");
        this.back.setText("Back");
        this.VS.setText("vs");
        this.VS.setFont(Font.font ("arial", 24));
        this.weekNumber.setFont(Font.font ("arial", 24));
        this.homeTeam.setFont(Font.font ("arial", 24));
        this.awayTeam.setFont(Font.font ("arial", 24));

    }
    public void addText(GameVisualObject game)
    {

        this.awayTeam.setText(game.getAwayTeam());
        this.homeTeam.setText(game.getHomeTeam());
        this.weekNumber.setText(weekNumber.getText());
        RegularSeasonModel.setRoundNumber(round);
        RegularSeasonModel.setUserDateFile(isPlayoff);
        this.boxScore = RegularSeasonModel.loadSchedule().findGame(homeTeam.getText(), awayTeam.getText()).getBoxScore();
        if(this.boxScore.getBoxScore().values().stream().allMatch(score->score.isComplete() == true))
        {
            this.addBoxScore.setDisable(true);
        }

        create.TableViewDefaultDecorator(vbox, Inning.class);

        create.populateTable(Arrays.asList(boxScore.getBoxScore().values().toArray()));
        create.getTable().setEditable(true);

    }

    public void onBackClick(ActionEvent actionEvent) throws IOException {
        Stage stage = (Stage) awayTeam.getScene().getWindow();
        FXMLLoader fxmlLoader;
        RegularSeasonModel.setUserDateFile(isPlayoff);
        if(isPlayoff)
        {
            fxmlLoader = new FXMLLoader(SoftballRecLeagueManager.class.getResource("controllers/ViewPlayoffSchedule-View.fxml"));
            stage.getScene().setRoot(fxmlLoader.load());
            ViewPlayoffScheduleController controller = fxmlLoader.getController();
            controller.setSchedule(RegularSeasonModel.loadSchedule());
        }
        else
        {
            fxmlLoader = new FXMLLoader(SoftballRecLeagueManager.class.getResource("controllers/ViewSchedule-View.fxml"));
            stage.getScene().setRoot(fxmlLoader.load());
            ViewScheduleController controller = fxmlLoader.getController();
            controller.setSchedule(RegularSeasonModel.loadSchedule());
        }





    }
    public void onAddScore() throws IOException {
        Stage stage = new Stage();
        Scene scene = new Scene(new GridPane());
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initOwner(this.awayTeam.getScene().getWindow());
        stage.setScene(scene);
        FXMLLoader fxmlLoader = new FXMLLoader(SoftballRecLeagueManager.class.getResource("controllers/AddScore-View.fxml"));;
        stage.getScene().setRoot(fxmlLoader.load());
        AddScoreController controller = fxmlLoader.getController();
       controller.setHomeTeam(this.homeTeam.getText());
       controller.setAwayTeam(this.awayTeam.getText());
       controller.setParentController(this);
       this.controller = controller;

        stage.showAndWait();
    }
    public void setGame(Game game)
    {
        this.game = game;
    }

    public AddScoreController getController() {
        return controller;
    }

    @Override
    public void notify(BoxScore boxscore) {

        create.populateTable(Arrays.asList(boxscore.getBoxScore().values().toArray()));
        create.getTable().setEditable(true);
        this.vbox.getChildren().clear();
        this.vbox.getChildren().add(create.getTable());

        if(boxscore.getBoxScore().values().stream().allMatch(score->score.isComplete() == true))
        {
            this.addBoxScore.setDisable(true);
            boxscore.sumHits();
        }

    }

    public boolean isPlayoff() {
        return isPlayoff;
    }

    public void setPlayoff(boolean playoff) {
        isPlayoff = playoff;
    }

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }
}

