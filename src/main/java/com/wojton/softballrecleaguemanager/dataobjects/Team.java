package com.wojton.softballrecleaguemanager.dataobjects;

import java.util.ArrayList;
import java.util.List;


public class Team {

    private String name;
    private String mascot;
    private String owner;
    private ArrayList<Player> availablePlayers;
   // private HashMap<String, Game> teamSchedule;
    private List<Week> weeks = new ArrayList<>();


    //line up of players goes here
    public Team()
    {

    }

    public Team(String name, String mascot, String owner){
        this.name = name;
        this.mascot = mascot;
        this.owner = owner;
        this.availablePlayers = new ArrayList<>();
    }
    public List<Player> getAvailablePlayers()
    {
        return this.availablePlayers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMascot() {
        return mascot;
    }

    public void setMascot(String mascot) {
        this.mascot = mascot;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public List<Week> getWeeks() {
        return weeks;
    }

    public void setWeeks(List<Week> weeks) {
        this.weeks = weeks;
    }
}
