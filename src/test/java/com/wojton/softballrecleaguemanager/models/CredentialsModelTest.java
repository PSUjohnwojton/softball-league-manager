package com.wojton.softballrecleaguemanager.models;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;


class CredentialsModelTest extends CredentialsModel{

    String userDataPath = System.getenv("LOCALAPPDATA") + "/data/userData";
    String userDateFile = userDataPath + "/" + "credentials.txt";
    String accountType = "league manager";
    @BeforeEach
    void setUp() {
        File myObj = new File(userDateFile);
        myObj.delete();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void saveOneSetCredentials() throws IOException {
        String username = "";
        String password = "";
        this.saveCredentials("username", "password", accountType);
        try {
            File myObj = new File(userDateFile);
            Scanner myReader = new Scanner(myObj);
            ArrayList<String> data = new ArrayList<>();
            while (myReader.hasNextLine()) {

                data.add(myReader.nextLine());
            }
            myReader.close();
            username = data.get(0).split(",")[0];
            password = data.get(0).split(",")[1];



        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        Assertions.assertTrue(username.equals("username"));
        Assertions.assertTrue(password.equals("password"));

    }
    @Test
    void saveTwoSetCredentials() throws IOException {
        this.saveCredentials("username", "password", accountType);
        this.saveCredentials("othername", "otherpass", accountType);
        try {
            File myObj = new File(userDateFile);
            Scanner myReader = new Scanner(myObj);
            ArrayList<String> data = new ArrayList<>();
            while (myReader.hasNextLine()) {

                data.add(myReader.nextLine());
            }
            myReader.close();


            assert(data.get(0).split(",")[0].equals("username"));
            assert(data.get(0).split(",")[1].equals("password"));
            assert(data.get(1).split(",")[0].equals("othername"));
            assert(data.get(1).split(",")[1].equals("otherpass"));

        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

    }
    @Test
    void userNameAlreadyExists() throws IOException {
       Assertions.assertTrue(this.saveCredentials("username", "password", accountType));
       Assertions.assertFalse(this.saveCredentials("username", "randompass", accountType));


    }

    @Test
    void testReadOneSetOfCredentials() throws IOException {
        this.saveCredentials("username", "password", accountType);
        Assertions.assertTrue(this.loadCredentials("username", "password"));

    }
    @Test
    void testReadReturnsFalseIfFileNotFound() throws IOException {

        Assertions.assertFalse(this.loadCredentials("username", "password"));

    }
    @Test
    void testReadReturnsFalse() throws IOException {
        this.saveCredentials("wrong", "error", accountType);
        Assertions.assertFalse(this.loadCredentials("username", "password"));

    }
    @Test
    void testMultipleLookupsMultipleEntries() throws IOException {
        for(int i = 0; i< 50; i++)
        {
            this.saveCredentials("wrong" + i, "error" + i, accountType);
        }
        for(int i = 0; i< 50; i++)
        {
            Assertions.assertTrue(this.loadCredentials("wrong" + i, "error" + i));
        }
        Assertions.assertFalse(this.loadCredentials("nothere", "alsonothere"));
    }
    @Test
    void testCorrectUsernameIncorrectPassword() throws IOException {
        this.saveCredentials("username", "password", accountType);
        Assertions.assertFalse(this.loadCredentials("username", "pass0rd"));
    }
}