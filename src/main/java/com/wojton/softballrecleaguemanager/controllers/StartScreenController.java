package com.wojton.softballrecleaguemanager.controllers;

import com.wojton.softballrecleaguemanager.SoftballRecLeagueManager;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;

public class StartScreenController {

    @FXML
    Label welcomeText;



    @FXML
    public void initialize() {
        welcomeText.setText("Welcome to the Softball Rec League Manager!");
    }

    @FXML
    protected void onLogInClick() throws IOException {
        Stage stage = (Stage) welcomeText.getScene().getWindow();
        // these two of them return the same stage
        // Swap screen
        System.out.println(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath());
        System.out.println(SoftballRecLeagueManager.class.getResource("controllers/SignIn-view.fxml"));
        FXMLLoader fxmlLoader = new FXMLLoader(SoftballRecLeagueManager.class.getResource("controllers/SignIn-view.fxml"));
        stage.getScene().setRoot(fxmlLoader.load());


    }


    @FXML
    protected void onSignUpClick() throws IOException {
        Stage stage = (Stage) welcomeText.getScene().getWindow();
        // these two of them return the same stage
        // Swap screen
        FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("SignUp-view.fxml"));
        stage.getScene().setRoot(fxmlLoader.load());
    }

}
