package com.wojton.softballrecleaguemanager.models;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wojton.softballrecleaguemanager.dataobjects.Schedule;
import com.wojton.softballrecleaguemanager.dataobjects.Team;
import com.wojton.softballrecleaguemanager.seasoncreators.PlayOffSeasonCreator;
import com.wojton.softballrecleaguemanager.seasoncreators.regularSeasonCreator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class RegularSeasonModel extends Model{
    static boolean isPlayoffs = false;
    static private int round = 1;
    static private int totalRounds = 1;
    static private String regualarSeason = "Schedules.txt";
    static private String playoffs = Integer.toString(round) + "PlayoffSchedule.json";
     static protected String userDataPath = System.getenv("LOCALAPPDATA") + "/data/userData";
     static protected String userDateFile = userDataPath + "/" + "Schedules.txt";

    static final private Gson gson = new GsonBuilder().setPrettyPrinting().create();
    public static String getUserDateFile()
    {
        return userDateFile;
    }
    public static Schedule getSchedule(ArrayList<Team> teams){
        Schedule schedule = loadSchedule();
    if(teams!= null) {
        if (schedule == null || (teams != null && !checkSchedule(schedule, teams))) {
            if (!isPlayoffs) {
                schedule = new regularSeasonCreator().createSchedule((teams));
            } else {
                PlayOffSeasonCreator creator = new PlayOffSeasonCreator(StandingsModel.loadStandings());
                schedule = creator.createSchedule(teams);
            }

        }
    }
        return schedule;
    }
    protected void setUserDataFile(String file)
    {
        userDateFile = userDataPath + "/" + file;
    }

    private static boolean checkSchedule(Schedule schedule, ArrayList<Team> teams)
    {
        AtomicBoolean contains = new AtomicBoolean(true);
        ArrayList<Team> teamsInSchedule = new ArrayList<Team>();

        schedule.getSchedule().stream().forEach(game ->
        {
            teamsInSchedule.add(game.getAwayTeam());
            teamsInSchedule.add(game.getHomeTeam());
        });
        for(Team team: teams)
        {

            contains.set(teamsInSchedule.stream().map(team1-> team1.getName()).collect(Collectors.toList()).contains(team.getName()));
            if(contains.get() == false)
            {
                return contains.get();
            }
        }

       return contains.get();

    }
    public static Schedule loadSchedule()
    {

       return gson.fromJson(readFile(userDateFile), Schedule.class);
    }
    public static void saveSchedule(Schedule schedule) throws IOException {
        checkFile(userDataPath, userDateFile);
        Schedule schedule1  = loadSchedule();
        if(schedule1 != null)
        {
            File scheduleFile = new File(userDateFile);
            scheduleFile.delete();
        }

        try (FileWriter writer = new FileWriter(userDateFile, false)) {
            try
            {
                writer.write(gson.toJson(schedule));
                writer.flush();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }

        }

    }
    public static void setUserDateFile(boolean playOffs)
    {
        isPlayoffs = playOffs;
        if(playOffs)
        {
            userDateFile = userDataPath + "/" + playoffs;
        }
        else
        {
            userDateFile = userDataPath + "/" + regualarSeason;
        }
    }
    public static void setRoundNumber(int roundNumber)
    {
            if(roundNumber > totalRounds)
            {
                totalRounds = roundNumber;
            }
            round = roundNumber;
        playoffs = Integer.toString(round) + "PlayoffSchedule.json";
        userDateFile = userDataPath + "/" + playoffs;
        System.out.println("round num " + roundNumber + " playoff num " + playoffs + " from model: " + getUserDateFile());
    }
    public static int getTotalRounds()
    {
        return totalRounds;
    }
    public static int getRound()
    {
        return round;
    }

    public static int getPlayOffFilNum()
    {
        ArrayList<String> names = new ArrayList<String>(List.of(new File(userDataPath).list()));
        return (int) names.stream().filter(name-> name.contains("PlayoffSchedule")).count();
    }
}
