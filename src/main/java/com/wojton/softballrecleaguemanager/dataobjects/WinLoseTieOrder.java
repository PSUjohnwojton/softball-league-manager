package com.wojton.softballrecleaguemanager.dataobjects;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class WinLoseTieOrder {
    public List<WinsLoseTie> orderStandings(Standings standings)
    {

          return standings.getStandingsPlain().values().stream().filter(standing ->
                      standing.getWins() > 0
                      || standing.getTies() > 0
                      || standing.getLoses() > 0).sorted(Comparator.comparing(WinsLoseTie::getWins)
                    .thenComparing(WinsLoseTie::getTies).thenComparing(WinsLoseTie::getLoses, Comparator.reverseOrder()).reversed()).collect(Collectors.toList());


    }
}
