package com.wojton.softballrecleaguemanager.dataobjects;

import java.util.HashMap;

public class BoxScore {
    private Team homeTeam;
    private Team awayTeam;
    private int homeScore = 0;
    private int awayScore = 0;
    private HashMap <String, Inning> boxScore = new HashMap<>();
    public BoxScore(Team awayTeam, Team homeTeam)
    {
        this.awayTeam = awayTeam;
        this.homeTeam = homeTeam;
        for(int i = 1; i <= 9; i++)
        {
            Inning inning = new Inning(i);
            boxScore.put(Integer.toString(i), inning);
        }

    }

    public HashMap<String, Inning> getBoxScore() {
        return boxScore;
    }
    public void sumHits()
    {
        this.homeScore =
                this.getBoxScore().values().stream().mapToInt(inning -> inning.getHomeRuns()).sum();
        this.awayScore =
                this.getBoxScore().values().stream().mapToInt(inning -> inning.getAwayRuns()).sum();
    }
    public void addInning(Inning inning)
    {
        this.getBoxScore().put(Integer.toString(inning.getInningNum()), inning);
    }

    public void setBoxScore(HashMap<String, Inning> boxScore) {
        this.boxScore = boxScore;
    }

    public Team getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    public Team getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(Team awayTeam) {
        this.awayTeam = awayTeam;
    }

    public int getHomeScore() {
        return homeScore;
    }

    public int getAwayScore() {
        return awayScore;
    }
}
