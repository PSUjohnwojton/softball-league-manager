package com.wojton.softballrecleaguemanager.controllers;

import com.wojton.softballrecleaguemanager.SoftballRecLeagueManager;
import com.wojton.softballrecleaguemanager.TableViewDecorator;
import com.wojton.softballrecleaguemanager.dataobjects.*;
import com.wojton.softballrecleaguemanager.observers.BoxScoreObserver;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ViewScheduleController {
    @FXML
    private VBox vBox;
    @FXML
    private Button backButton;
    @FXML
    private Button viewGameButton;
    private TableViewDecorator decorator;
    private GameViewController controller;
    private List<BoxScoreObserver> observers = new ArrayList<>();


    @FXML
    public void initialize() {

        backButton.setText("back");
        viewGameButton.setText("View Selected Game");

    }
    public void setSchedule(Schedule schedule)
    {
        HashMap<String, String> map = new HashMap<>();
        map.put("Away Team", "awayTeam");
        map.put("Home Team", "homeTeam");
        map.put("Week", "weekNumber");
        decorator = new TableViewDecorator();
        decorator.TableCustomColDecorator(vBox, map);
        ArrayList<GameVisualObject> gameVisualObjectArrayList = new ArrayList<>();

        schedule.getSchedule().stream().forEach(game->{
            gameVisualObjectArrayList.add(new GameVisualObject(game.getAwayTeam().getName(), game.getHomeTeam().getName(), game.getWeekNumber()));
        });
        decorator.populateTable(gameVisualObjectArrayList);

    }
    public void onBackButton() throws IOException {
        Stage stage = (Stage) vBox.getScene().getWindow();
        FXMLLoader fxmlLoader = new FXMLLoader(SoftballRecLeagueManager.class.getResource("controllers/LeagueManager-View.fxml"));
        stage.getScene().setRoot(fxmlLoader.load());
    }
    public void onViewGameButton() throws IOException {
      GameVisualObject game = (GameVisualObject)  decorator.getTable().getSelectionModel().getSelectedItem();
        Stage stage = (Stage) vBox.getScene().getWindow();
        FXMLLoader fxmlLoader = new FXMLLoader(SoftballRecLeagueManager.class.getResource("controllers/Game-View.fxml"));

        stage.getScene().setRoot(fxmlLoader.load());
        this.controller = (GameViewController)fxmlLoader.getController();
        this.controller.addText(game);
      //  this.observers.stream().forEach(this.controller.getController()::addObserver);

    }
    public void addObserver(BoxScoreObserver observer)
    {
        this.observers.add(observer);
      //  this.controller.getController().addObserver(observer);
    }
    public TableViewDecorator getDecorator()
    {
        return decorator;
    }
}
