package com.wojton.softballrecleaguemanager.observers;

import com.wojton.softballrecleaguemanager.dataobjects.BoxScore;
import com.wojton.softballrecleaguemanager.mockBoxScoreSubject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BoxScoreObserverTest implements BoxScoreObserver {

    private mockBoxScoreSubject subject = new mockBoxScoreSubject();
    private BoxScore returnedBoxScore;
    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testNotifyNotObserver() {
        BoxScore expected = subject.getBoxScore();
        Assertions.assertNull( returnedBoxScore);
    }
    @Test
    void testNotifyObserver() {
        subject.addObserver(this);
        BoxScore expected = subject.getBoxScore();
        Assertions.assertEquals(expected, returnedBoxScore);
    }
    @Test
    void testNotifyNullAfterRemoveObserver() {
        subject.addObserver(this);
        BoxScore expected = subject.getBoxScore();
        Assertions.assertEquals(expected, returnedBoxScore);
        returnedBoxScore = null;
        subject.removeObserver(this);
        expected = subject.getBoxScore();
        Assertions.assertNull(returnedBoxScore);
    }

    @Override
    public void notify(BoxScore boxscore) {
            returnedBoxScore = boxscore;
    }
}