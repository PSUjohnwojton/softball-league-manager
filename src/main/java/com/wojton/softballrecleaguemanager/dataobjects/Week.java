package com.wojton.softballrecleaguemanager.dataobjects;

public class Week {
    int week;
    String opponent;
    public Week(int week, String opponent)
    {
        this.week = week;
        if(opponent != null) {
            this.opponent = opponent;
        }
    }
    public int getWeek()
    {
        return this.week;
    }
    public void setWeek(int week)
    {
        this.week = week;
    }
    public String getOpponent()
    {
        return this.opponent;
    }
    public void setOpponent(String opponent)
    {
        this.opponent = opponent;
    }
}
