package com.wojton.softballrecleaguemanager.dataobjects;

public class Inning {
    private int inningNum;
    private int homeErrors;
    private int homeRuns;
    private int homeHits;
    private int awayErrors;
    private int awayRuns;
    private int awayHits;
    private boolean complete;
    public Inning(int inningNum)
    {
        this.complete = false;
        this.inningNum = inningNum;
        homeErrors = 0;
        homeRuns = 0;
        homeHits = 0;
        awayErrors = 0;
        awayHits = 0;
        awayRuns = 0;
    }

    public int getHomeErrors() {
        return homeErrors;
    }

    public void setHomeErrors(int homeErrors) {
        this.homeErrors = homeErrors;
    }

    public int getHomeRuns() {
        return homeRuns;
    }

    public void setHomeRuns(int homeRuns) {
        this.homeRuns = homeRuns;
    }

    public int getHomeHits() {
        return homeHits;
    }

    public void setHomeHits(int homeHits) {
        this.homeHits = homeHits;
    }

    public int getAwayErrors() {
        return awayErrors;
    }

    public void setAwayErrors(int awayErrors) {
        this.awayErrors = awayErrors;
    }

    public int getAwayRuns() {
        return awayRuns;
    }

    public void setAwayRuns(int awayRuns) {
        this.awayRuns = awayRuns;
    }

    public int getAwayHits() {
        return awayHits;
    }

    public void setAwayHits(int awayHits) {
        this.awayHits = awayHits;
    }

    public int getInningNum() {
        return inningNum;
    }

    public void setInningNum(int inningNum) {
        this.inningNum = inningNum;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }
}
