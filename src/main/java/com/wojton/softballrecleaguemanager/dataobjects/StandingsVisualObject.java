package com.wojton.softballrecleaguemanager.dataobjects;

public class StandingsVisualObject {
    private String team;
    private int wins;
    private int loses;
    private int ties;
   public StandingsVisualObject(String team, int wins, int loses, int ties){
        this.team = team;
        this.wins = wins;
        this.loses = loses;
        this.ties = ties;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getLoses() {
        return loses;
    }

    public void setLoses(int loses) {
        this.loses = loses;
    }

    public int getTies() {
        return ties;
    }

    public void setTies(int ties) {
        this.ties = ties;
    }
}
