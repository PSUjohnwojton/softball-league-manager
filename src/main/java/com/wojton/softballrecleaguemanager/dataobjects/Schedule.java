package com.wojton.softballrecleaguemanager.dataobjects;

import java.util.ArrayList;

public class Schedule {
    protected ArrayList<Game> schedule;
    public Game findGame(String team1 , String team2)
    {
        if(schedule!= null)
        {
           return schedule.stream().filter(game -> game.getAwayTeam().getName().equals(team2) || game.getHomeTeam().getName().equals(team2))
                    .filter(game -> game.getHomeTeam().getName().equals(team1) || game.getAwayTeam().getName().equals(team1)).findFirst().get();
        }
        System.out.println("Could not find game with team1 : " + team1 + " and tea2 : " + team2);
        return null;
    }

    public ArrayList<Game> getSchedule() {
        if(this.schedule == null)
        {
            this.schedule = new ArrayList<>();
        }
        return schedule;
    }

    public void setSchedule(ArrayList<Game> schedule) {

        this.schedule = schedule;
    }
}
