module com.wojton.softballrecleaguemanager {


    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires validatorfx;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    requires eu.hansolo.tilesfx;
    requires com.google.gson;



    opens com.wojton.softballrecleaguemanager;
    exports com.wojton.softballrecleaguemanager.models;
    opens com.wojton.softballrecleaguemanager.models;
    exports com.wojton.softballrecleaguemanager.controllers;
    opens com.wojton.softballrecleaguemanager.controllers to javafx.fxml;
    exports com.wojton.softballrecleaguemanager.seasoncreators;
    opens com.wojton.softballrecleaguemanager.seasoncreators;
    exports com.wojton.softballrecleaguemanager;
    opens com.wojton.softballrecleaguemanager.observers;
    exports com.wojton.softballrecleaguemanager.observers;
    opens com.wojton.softballrecleaguemanager.dataobjects;
    exports com.wojton.softballrecleaguemanager.dataobjects;
}