package com.wojton.softballrecleaguemanager.observers;

import com.wojton.softballrecleaguemanager.dataobjects.BoxScore;

public interface BoxScoreObserver {
    void notify(BoxScore boxscore);
}
