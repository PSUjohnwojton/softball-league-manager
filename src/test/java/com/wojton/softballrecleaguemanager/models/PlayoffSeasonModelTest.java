package com.wojton.softballrecleaguemanager.models;

import com.wojton.softballrecleaguemanager.dataobjects.BoxScore;
import com.wojton.softballrecleaguemanager.dataobjects.Schedule;
import com.wojton.softballrecleaguemanager.dataobjects.Team;
import com.wojton.softballrecleaguemanager.observers.BoxScoreObserver;
import com.wojton.softballrecleaguemanager.seasoncreators.regularSeasonCreator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class PlayoffSeasonModelTest {

    private PlayoffSeasonModel model = new PlayoffSeasonModel();
    private Schedule schedule;
    private BoxScore boxScore;
    private HashMap<String, Team> teamsMap = new HashMap<>();
    static private String regualarSeason = "Schedules.txt";
    static private String playoffs = "1PlayoffSchedule.json";
    static protected String userDataPath = System.getenv("LOCALAPPDATA") + "/data/userData";
    static protected String userDateFile = userDataPath + "/" + "Schedules.txt";
    @BeforeEach
    void before() throws IOException {
        RegularSeasonModel.setUserDateFile(true);
        File myObj = new File(RegularSeasonModel.getUserDateFile());
        myObj.delete();
        ArrayList<Team> teams = new ArrayList<>();
        for(int i = 0; i<4; i++)
        {

            Team team = new Team();
            team.setName(Integer.toString(i));
            team.setOwner(Integer.toString(i));
            team.setMascot(Integer.toString(i));
            teams.add(team);
            teamsMap.put(Integer.toString(i), team);

        }
        regularSeasonCreator creator = new regularSeasonCreator();
        RegularSeasonModel.saveSchedule(creator.createSchedule(teams));
        schedule = RegularSeasonModel.getSchedule(teams);
        Team away = new Team();
        away.setMascot("1");
        away.setOwner("1");
        away.setName("1");
        Team home = new Team();
        home.setMascot("2");
        home.setOwner("2");
        home.setName("2");
        boxScore = new BoxScore(away, home);

    }
    @Test
    void testModel() throws IOException {
        RegularSeasonModel.setRoundNumber(1);
        RegularSeasonModel.setUserDateFile(true);

        RegularSeasonModel.saveSchedule(schedule);
        Schedule loadedSchedule = RegularSeasonModel.loadSchedule();
        Assertions.assertEquals( userDataPath + "/" + playoffs, RegularSeasonModel.getUserDateFile());
        Assertions.assertNotNull(loadedSchedule);
    }

}