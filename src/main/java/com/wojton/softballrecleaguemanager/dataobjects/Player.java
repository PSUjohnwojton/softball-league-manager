package com.wojton.softballrecleaguemanager.dataobjects;

public class Player {
    protected String name;
    protected int number;
    protected Position preferedPosition;

    void Player(String name, int number, Position preferedPosition)
    {
        this.name = name;
        this.number = number;
        this.preferedPosition = preferedPosition;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Position getPreferedPosition() {
        return preferedPosition;
    }

    public void setPreferedPosition(Position preferedPosition) {
        this.preferedPosition = preferedPosition;
    }
}
