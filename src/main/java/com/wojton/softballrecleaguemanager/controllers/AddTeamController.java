package com.wojton.softballrecleaguemanager.controllers;

import com.wojton.softballrecleaguemanager.SoftballRecLeagueManager;
import com.wojton.softballrecleaguemanager.dataobjects.Team;
import com.wojton.softballrecleaguemanager.models.TeamModel;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;

public class AddTeamController {

    private TeamModel teamModel;
    @FXML
    Label teamName;
    @FXML
    TextField teamNameField;
    @FXML
    Label owner;
    @FXML
    TextField ownerField;
    @FXML
    Label mascot;
    @FXML
    TextField mascotField;
    @FXML
    Button cancelButton;
    @FXML
    Button saveButton;
    @FXML
    GridPane gridPane;

    @FXML
    public void initialize() {
        teamName.setText("Name: ");
        owner.setText("Owner: ");
        mascot.setText("Mascot: ");
        cancelButton.setText("Cancel");
        saveButton.setText("Save");
        teamModel = new TeamModel();
        gridPane.setBorder(new Border(new BorderStroke(Color.BLACK,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));

    }

    public void onCancelButton() throws IOException {
        Stage stage = (Stage) teamNameField.getScene().getWindow();
        FXMLLoader fxmlLoader = new FXMLLoader(SoftballRecLeagueManager.class.getResource("controllers/LeagueManager-View.fxml"));
        stage.getScene().setRoot(fxmlLoader.load());
    }
    public void onSaveButton() throws IOException {
        Team team = new Team(teamNameField.getText(), mascotField.getText(), ownerField.getText());
        teamModel.saveTeam(team);
        Stage stage = (Stage) teamNameField.getScene().getWindow();
        FXMLLoader fxmlLoader = new FXMLLoader(SoftballRecLeagueManager.class.getResource("controllers/LeagueManager-View.fxml"));
        stage.getScene().setRoot(fxmlLoader.load());
    }

}
