package com.wojton.softballrecleaguemanager.seasoncreators;

import com.wojton.softballrecleaguemanager.dataobjects.Schedule;
import com.wojton.softballrecleaguemanager.dataobjects.Team;

import java.util.ArrayList;

public interface scheduleCreationAlgorithm {

    Schedule createSchedule(ArrayList<Team> teams);
}
