package com.wojton.softballrecleaguemanager.controllers;

import com.wojton.softballrecleaguemanager.SoftballRecLeagueManager;
import com.wojton.softballrecleaguemanager.TableViewDecorator;
import com.wojton.softballrecleaguemanager.dataobjects.Standings;
import com.wojton.softballrecleaguemanager.dataobjects.StandingsVisualObject;
import com.wojton.softballrecleaguemanager.dataobjects.WinLoseTieOrder;
import com.wojton.softballrecleaguemanager.models.RegularSeasonModel;
import com.wojton.softballrecleaguemanager.models.TeamModel;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

public class ViewStandingsController {
    @FXML
    VBox vBox;
    @FXML
    Button backButton;
    Standings standings = new Standings( TeamModel.loadTeams());

    @FXML
    void initialize() throws IOException {
        backButton.setText("Back");
        RegularSeasonModel.setUserDateFile(false);
        standings.setStandings(RegularSeasonModel.loadSchedule());
        WinLoseTieOrder orderer = new WinLoseTieOrder();
        ArrayList<StandingsVisualObject> visualObjects = new ArrayList<>();
        orderer.orderStandings(standings).stream().forEach(wlt -> {
            visualObjects.add(new StandingsVisualObject(wlt.getTeamName(),wlt.getWins(), wlt.getLoses(), wlt.getTies()));
        });
        TableViewDecorator decorator = new TableViewDecorator();
        decorator.TableViewDefaultDecorator(vBox, StandingsVisualObject.class);
        decorator.populateTable(visualObjects);
     //   new PlayOffSeasonCreator(standings).createSchedule(new ArrayList<>());
    }

    @FXML
    void onBackButton() throws IOException {
        Stage stage = (Stage) vBox.getScene().getWindow();
        FXMLLoader fxmlLoader = new FXMLLoader(SoftballRecLeagueManager.class.getResource("controllers/LeagueManager-view.fxml"));
        stage.getScene().setRoot(fxmlLoader.load());
    }
}
