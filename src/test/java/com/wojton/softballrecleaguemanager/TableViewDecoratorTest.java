package com.wojton.softballrecleaguemanager;

import com.wojton.softballrecleaguemanager.dataobjects.Player;
import com.wojton.softballrecleaguemanager.dataobjects.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.*;

class TableViewDecoratorTest {

    TableViewDecorator decorator = new TableViewDecorator();
    @Test
    void getColumnNamesPlayerTest() {
       List<Field> fields = Arrays.asList(Player.class.getDeclaredFields());
       ArrayList<String> expectedNames = (ArrayList<String>) fields.stream().map(Field::getName).collect(Collectors.toList());
       List<Field> names = decorator.getColumnNames(Player.class);
       names.stream().forEach(field->{
           Assertions.assertTrue(expectedNames.contains( field.getName()));
       });
    }
    @Test
    void getColumnNamesTeamTest() {
        List<Field> fields = Arrays.asList(Team.class.getDeclaredFields());
        ArrayList<String> expectedNames = (ArrayList<String>) fields.stream().map(Field::getName).collect(Collectors.toList());
        List<Field> names = decorator.getColumnNames(Team.class);
        names.stream().forEach(field->{
            Assertions.assertTrue(expectedNames.contains( field.getName()));
        });
    }
}