package com.wojton.softballrecleaguemanager.models;

import com.wojton.softballrecleaguemanager.dataobjects.BoxScore;
import com.wojton.softballrecleaguemanager.dataobjects.Schedule;
import com.wojton.softballrecleaguemanager.dataobjects.Standings;
import com.wojton.softballrecleaguemanager.dataobjects.Team;
import com.wojton.softballrecleaguemanager.observers.BoxScoreObserver;
import com.wojton.softballrecleaguemanager.seasoncreators.regularSeasonCreator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class StandingsModelTest  {
    BoxScore boxScore;
    Schedule schedule;
    Map<String, Team> teamsMap = new HashMap<>();
    final private String userDataPath = System.getenv("LOCALAPPDATA") + "/data/userData";
    final private String userDateFile = userDataPath + "/" + "standings.txt";
    @BeforeEach
    void before()
    {
        ArrayList<Team> teams = new ArrayList<>();
        for(int i = 0; i<4; i++)
        {

            Team team = new Team();
            team.setName(Integer.toString(i));
            team.setOwner(Integer.toString(i));
            team.setMascot(Integer.toString(i));
            teams.add(team);
            teamsMap.put(Integer.toString(i), team);

        }
        regularSeasonCreator creator = new regularSeasonCreator();
        creator.createSchedule(teams);
        schedule = RegularSeasonModel.getSchedule(teams);
        Team away = new Team();
        away.setMascot("1");
        away.setOwner("1");
        away.setName("1");
        Team home = new Team();
        home.setMascot("2");
        home.setOwner("2");
        home.setName("2");
        boxScore = new BoxScore(away, home);

    }

    @Test
    void saveStandings() throws IOException {
        Standings standings = new Standings(teamsMap);
        StandingsModel.saveStandings(standings);
        String result = StandingsModel.readFile(userDateFile);
        Assertions.assertNotEquals("", result);
    }

    @Test
    void loadStandings() throws IOException {
        Standings standings = new Standings(teamsMap);
        StandingsModel.saveStandings(standings);
        Standings standings1 = StandingsModel.loadStandings();
        teamsMap.keySet().stream().forEach(key ->
        {
            Assertions.assertNotNull(standings1.getStandingsMap().get(key));
            Assertions.assertTrue(standings1.getStandingsMap().get(key).equals(standings.getStandingsMap().get(key)));
        });
    }
}