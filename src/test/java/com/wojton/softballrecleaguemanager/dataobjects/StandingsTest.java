package com.wojton.softballrecleaguemanager.dataobjects;

import com.wojton.softballrecleaguemanager.models.RegularSeasonModel;
import com.wojton.softballrecleaguemanager.models.StandingsModel;
import com.wojton.softballrecleaguemanager.seasoncreators.regularSeasonCreator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;


class StandingsTest {
    BoxScore boxScore;
    Schedule schedule;
    Map<String, Team> teamsMap = new HashMap<>();
    @BeforeEach
    void before() throws IOException {

        File myObj = new File(RegularSeasonModel.getUserDateFile());
        myObj.delete();
        myObj = new File(StandingsModel.getUserDateFile());
        myObj.delete();
        ArrayList<Team> teams = new ArrayList<>();
        for(int i = 0; i<4; i++)
        {

            Team team = new Team();
            team.setName(Integer.toString(i));
            team.setOwner(Integer.toString(i));
            team.setMascot(Integer.toString(i));
            teams.add(team);
            teamsMap.put(Integer.toString(i), team);

        }
        regularSeasonCreator creator = new regularSeasonCreator();
        RegularSeasonModel.saveSchedule(creator.createSchedule(teams));
        schedule = RegularSeasonModel.getSchedule(teams);
        Team away = new Team();
        away.setMascot("1");
        away.setOwner("1");
        away.setName("1");
        Team home = new Team();
        home.setMascot("2");
        home.setOwner("2");
        home.setName("2");
        boxScore = new BoxScore(away, home);

    }
    @Test
    void testSetStandings() throws IOException {
        Standings standings = new Standings(teamsMap);
        standings.setStandings(RegularSeasonModel.getSchedule(new ArrayList<>(teamsMap.values())));
        Assertions.assertNotNull(StandingsModel.loadStandings());

    }
    @Test
    void testConstructorAddsTeams() {
        Standings standings = new Standings(teamsMap);
        teamsMap.values().stream().forEach(name ->
        {
            Assertions.assertNotNull(standings.getStandingsMap().get(name.getName()));
        });
    }
    @Test
    void testWinLoseAdded()
    {
        Standings standings = new Standings(teamsMap);
        HashMap<String, Inning> box = new HashMap<>();
        IntStream.range(1, 9).forEach(i -> {
            Inning inning = new Inning(i);
            inning.setComplete(true);
            inning.setHomeRuns(1);
            inning.setHomeHits(1);
            inning.setHomeErrors(1);
            inning.setAwayErrors(0);
            inning.setAwayHits(0);
            inning.setAwayRuns(0);
            box.put(Integer.toString(i), inning);


        });
        boxScore.setBoxScore(box);
        standings.addBoxScore(boxScore);
        Assertions.assertEquals(1, standings.getStandingsMap().get(Integer.toString(1)).getLoses());
        Assertions.assertEquals(0, standings.getStandingsMap().get(Integer.toString(1)).getTies());
        Assertions.assertEquals(0, standings.getStandingsMap().get(Integer.toString(1)).getWins());
        Assertions.assertEquals(0, standings.getStandingsMap().get(Integer.toString(2)).getLoses());
        Assertions.assertEquals(0, standings.getStandingsMap().get(Integer.toString(2)).getTies());
        Assertions.assertEquals(1, standings.getStandingsMap().get(Integer.toString(2)).getWins());
    }

    @Test
    void testTieAdded()
    {
        Standings standings = new Standings(teamsMap);
        HashMap<String, Inning> box = new HashMap<>();
        IntStream.range(1, 9).forEach(i -> {
            Inning inning = new Inning(i);
            inning.setComplete(true);
            inning.setHomeRuns(1);
            inning.setHomeHits(1);
            inning.setHomeErrors(1);
            inning.setAwayErrors(1);
            inning.setAwayHits(1);
            inning.setAwayRuns(1);
            box.put(Integer.toString(i), inning);


        });
        boxScore.setBoxScore(box);
        standings.addBoxScore(boxScore);
        Assertions.assertEquals(0, standings.getStandingsMap().get(Integer.toString(1)).getLoses());
        Assertions.assertEquals(1, standings.getStandingsMap().get(Integer.toString(1)).getTies());
        Assertions.assertEquals(0, standings.getStandingsMap().get(Integer.toString(1)).getWins());
        Assertions.assertEquals(0, standings.getStandingsMap().get(Integer.toString(2)).getLoses());
        Assertions.assertEquals(1, standings.getStandingsMap().get(Integer.toString(2)).getTies());
        Assertions.assertEquals(0, standings.getStandingsMap().get(Integer.toString(2)).getWins());
    }
}