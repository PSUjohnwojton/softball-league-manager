package com.wojton.softballrecleaguemanager.seasoncreators;

import com.wojton.softballrecleaguemanager.dataobjects.Schedule;
import com.wojton.softballrecleaguemanager.dataobjects.Team;
import com.wojton.softballrecleaguemanager.dataobjects.Game;
import com.wojton.softballrecleaguemanager.dataobjects.Week;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class regularSeasonCreator implements scheduleCreationAlgorithm {

    int i = 0;
    @Override
    public Schedule createSchedule(ArrayList<Team> teamList) {

        Schedule schedule = new Schedule();
        try {
        Random random = new Random();
        prepWeeks(teamList);

        ArrayList<Team> teams = teamList;;
        ArrayList<Team> finishedTeams = new ArrayList<>();
        ArrayList<Game> games = new ArrayList<>();

        teams.stream().forEach(team -> {
            teams.stream().forEach(team1 -> {
                if(team != team1 && !finishedTeams.contains(team1)) {
                    Game game = new Game();
                    if(random.nextBoolean())
                    {
                        game.setHomeTeam(team);
                        game.setAwayTeam(team1);
                    }
                    else
                    {
                        game.setHomeTeam(team1);
                        game.setAwayTeam(team);
                    }
                    game.setWeekNumber(addWeeksToSchedule(game).getWeek());
                    games.add(game);
                }
            });
            finishedTeams.add(team);
        });
        ArrayList<Game> newGames = new ArrayList<>();
        newGames.addAll(games);
        schedule.setSchedule( newGames);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return schedule;
    }
    protected void prepWeeks(List<Team> teams)
    {
        int numOfWeeks =  ((teams.size()/2)*(teams.size()-1));
        for(int i = 1; i <= numOfWeeks; i++)
        {
            int finalI = i;
            teams.stream().forEach(team -> team.getWeeks().add(new Week(finalI, "")));
        }
    }
    protected Week addWeeksToSchedule(Game game)
    {
        Week week = null;
       ArrayList<Week> homeWeeks = (ArrayList<Week>) game.getHomeTeam().getWeeks().stream().filter(week1 -> week1.getOpponent().equals("")).collect(Collectors.toList());
       ArrayList<Week> awayWeeks = (ArrayList<Week>) game.getAwayTeam().getWeeks().stream().filter(week1 -> week1.getOpponent().equals("")).collect(Collectors.toList());

        for (Week week1 :homeWeeks   ) {
            for (Week week2 : awayWeeks) {
                if(week1.getWeek() == week2.getWeek()){
                    week = new Week(week1.getWeek(), "");

                    break;
                }
            }
            if(week != null && week.getWeek() != 0)
            {
                break;
            }
        }

        Week finalWeek = week;
        game.getHomeTeam().getWeeks().stream().filter(week1 -> week1.getWeek() == finalWeek.getWeek()).findFirst().get().setOpponent(game.getAwayTeam().getName());
        game.getAwayTeam().getWeeks().stream().filter(week1 -> week1.getWeek() == finalWeek.getWeek()).findFirst().get().setOpponent(game.getHomeTeam().getName());
      System.out.println("This is the week! " + week);
      return week;
    }
}
