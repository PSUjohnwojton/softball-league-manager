package com.wojton.softballrecleaguemanager.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class SoftballRecLeagueManagerController {
    @FXML
    private Label welcomeText;

    @FXML
    protected void onHelloButtonClick() {
        welcomeText.setText("Welcome to JavaFX Application!");
    }
}