package com.wojton.softballrecleaguemanager.models;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.wojton.softballrecleaguemanager.dataobjects.Player;
import com.wojton.softballrecleaguemanager.dataobjects.Position;
import com.wojton.softballrecleaguemanager.dataobjects.Team;
import junit.framework.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class TeamModelTest extends TeamModel{

    String userDataPath = System.getenv("LOCALAPPDATA") + "/data/userData";
    String userDateFile = userDataPath + "/" + "teams.txt";
    Gson gson = new Gson();

    @BeforeEach
    void setUp() {
        File myObj = new File(userDateFile);
       boolean deleted = myObj.delete();
       System.out.println("Deleted: " + deleted);
    }

    @AfterEach
    void tearDown() {

    }
    @Test
    void checkFileMadeIfNotFoundTest() throws IOException {
        File dir = new File(userDataPath);
        File teamsFile = new File(userDateFile);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        Assertions.assertFalse(teamsFile.exists());
        String name = "testName";
        String mascot = "Bears";
        String owners = "billy";
        Team team = new Team(name, mascot, owners);
        this.saveTeam(team);
        Assertions.assertTrue(teamsFile.exists());
    }

    @Test
    void saveOneTeamTest() throws IOException {
        String name = "testName";
        String mascot = "Bears";
        String owners = "billy";
        Team team = new Team(name, mascot, owners);
        this.saveTeam(team);
        File myObj = new File(userDateFile);
        try {

            Scanner myReader = new Scanner(myObj);
            String data = "";
            while (myReader.hasNextLine()) {

                data += myReader.nextLine();
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();

        }
        Type TeamListType = new TypeToken<ArrayList<Team>>(){}.getType();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        HashMap<String,Team> teams = (HashMap<String,Team>) this.loadTeams();
        Assertions.assertNotNull(teams);
        Assertions.assertEquals(1, teams.size(), "Team size does not equal 1");
        Assertions.assertEquals(name, teams.get(name).getName());
        Assertions.assertEquals(mascot, teams.get(name).getMascot());
        Assertions.assertEquals(owners, teams.get(name).getOwner());
    }

    @Test
    void saveTwoTeamTest() throws IOException {
        String name = "name1";
        String mascot = "cheifs";
        String owners = "joe";
        String name2 = "testOtherName";
        String mascot2 = "wolfs";
        String owners2 = "tom";
        Team team = new Team(name, mascot, owners);
        Team team1 = new Team(name2, mascot2, owners2);
        this.saveTeam(team);
        this.saveTeam(team1);
        try {
            File myObj = new File(userDateFile);
            Scanner myReader = new Scanner(myObj);
            String data = "";
            while (myReader.hasNextLine()) {

                data += myReader.nextLine();
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        HashMap<String,Team> teams = this.loadTeams();
        Assertions.assertNotNull(teams);
        Assertions.assertEquals(2, teams.size(), "Team size does not equal 2");
        Assertions.assertEquals(name, teams.get(name).getName());
        Assertions.assertEquals(mascot, teams.get(name).getMascot());
        Assertions.assertEquals(owners, teams.get(name).getOwner());
        Assertions.assertEquals(name2, teams.get(name2).getName());
        Assertions.assertEquals(mascot2, teams.get(name2).getMascot());
        Assertions.assertEquals(owners2, teams.get(name2).getOwner());
    }
    @Test
    void loadOneTeam() throws IOException {
        String name = "name1";
        String mascot = "cheifs";
        String owners = "joe";
        Team team = new Team(name, mascot, owners);
        this.saveTeam(team);
        HashMap<String,Team> teams = this.loadTeams();
        Assertions.assertNotNull(teams);
        Assertions.assertEquals(name, teams.get(name).getName());
        Assertions.assertEquals(mascot, teams.get(name).getMascot());
        Assertions.assertEquals(owners, teams.get(name).getOwner());
    }

    @Test
    void loadTwoTeam() throws IOException {
        String name = "name1";
        String mascot = "cheifs";
        String owners = "joe";
        String name2 = "testOtherName";
        String mascot2 = "wolfs";
        String owners2 = "tom";
        Team team = new Team(name, mascot, owners);
        Team team2 = new Team(name2, mascot2, owners2);
        this.saveTeam(team);
        this.saveTeam(team2);
        HashMap<String,Team> teams = this.loadTeams();
        Assertions.assertNotNull(teams);
        Assertions.assertEquals(name, teams.get(name).getName());
        Assertions.assertEquals(mascot, teams.get(name).getMascot());
        Assertions.assertEquals(owners, teams.get(name).getOwner());
        Assertions.assertEquals(name2, teams.get(name2).getName());
        Assertions.assertEquals(mascot2, teams.get(name2).getMascot());
        Assertions.assertEquals(owners2, teams.get(name2).getOwner());
    }
    @Test
    void addPlayerToTeamTest() throws IOException {
        String name = "name1";
        String mascot = "cheifs";
        String owners = "joe";
        Team team = new Team(name, mascot, owners);
        String playerName = "joe";
        Position playerPosition = Position.FIRSTBASE;
        int number = 22;
        Player player = new Player();
        player.setName(playerName);
        player.setPreferedPosition(playerPosition);
        player.setNumber(number);
        team.getAvailablePlayers().add(player);
        this.saveTeam(team);
        Team loadedTeam = this.loadTeams().get(name);
        Assertions.assertEquals(name, loadedTeam.getName());
        Assertions.assertEquals(1, team.getAvailablePlayers().size());
        Assertions.assertEquals(playerName, team.getAvailablePlayers().get(0).getName());
        Assertions.assertEquals(playerPosition, team.getAvailablePlayers().get(0).getPreferedPosition());
        Assertions.assertEquals(number, team.getAvailablePlayers().get(0).getNumber());
    }

}