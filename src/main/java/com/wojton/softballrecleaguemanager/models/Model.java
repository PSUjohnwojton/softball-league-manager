package com.wojton.softballrecleaguemanager.models;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.util.Scanner;

public class Model {
    protected static void checkFile(String userDataPath, String userDateFile) throws IOException {
        File dir = new File(userDataPath);
        File credentials = new File(userDateFile);
        if(!dir.exists()) {
            dir.mkdirs();
        }
        if(!credentials.exists() && !credentials.createNewFile())
        {

            throw new NoSuchFileException(credentials.getAbsolutePath(), null, "File Can not be created");

        }
    }
    protected static String readFile(String userDateFile)
    {
        File myObj = new File(userDateFile);
        String data = "";
        try (Scanner myReader = new Scanner(myObj)) {

            while (myReader.hasNextLine()) {

                data+=myReader.nextLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return data;
    }
}
