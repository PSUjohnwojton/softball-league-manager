package com.wojton.softballrecleaguemanager.seasoncreators;

import com.wojton.softballrecleaguemanager.dataobjects.*;

import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PlayOffSeasonCreator implements scheduleCreationAlgorithm {

    private Standings standings;
    private WinLoseTieOrder orderer = new WinLoseTieOrder();
    public PlayOffSeasonCreator(Standings standings)
    {
        this.standings = standings;
    }
    @Override
    public Schedule createSchedule(ArrayList<Team> teams) {
       ArrayList<WinsLoseTie> orderStandings = (ArrayList) orderer.orderStandings(standings);
        ArrayList<Game> games = new ArrayList<>();
        if(teams.size() == orderStandings.size()) {
            int size = orderStandings.size() - 1;
            IntStream.range(0, orderStandings.size() / 2).forEach(i -> {
                Optional<Team> betterTeam = teams.stream().filter(team -> team.getName().equals(orderStandings.get(i).getTeamName())).findFirst();
                Optional<Team> worseTeam = teams.stream().filter(team -> team.getName().equals(orderStandings.get(size - i).getTeamName())).findFirst();
                if (betterTeam.isPresent() && worseTeam.isPresent()) {
                    Game game = new Game();
                    game.setHomeTeam(betterTeam.get());
                    game.setAwayTeam(worseTeam.get());
                    games.add(game);
                }



            });
        }
        else
        {

            ArrayList<String> names = (ArrayList<String>) teams.stream().map(team-> team.getName()).collect(Collectors.toList());
            ArrayList<String> filterStandings
                    = (ArrayList<String>) orderStandings.stream()
                    .map(standings-> standings.getTeamName()).filter(name->names.contains(name)).collect(Collectors.toList());
            int size = filterStandings.size() - 1;
            IntStream.range(0, size).forEach(i -> {
                Optional<Team> betterTeam = teams.stream().filter(team -> team.getName().equals(filterStandings.get(i))).findFirst();
                Optional<Team> worseTeam = teams.stream().filter(team -> team.getName().equals(filterStandings.get(size - i))).findFirst();
                if (betterTeam.isPresent() && worseTeam.isPresent()) {
                    Game game = new Game();
                    game.setHomeTeam(betterTeam.get());
                    game.setAwayTeam(worseTeam.get());

                    if(betterTeam.get().equals(worseTeam.get()))
                    {
                        BoxScore box = game.getBoxScore();
                        IntStream.range(1,10).forEach(j->{
                            Inning inn = new Inning(i);
                            inn.setComplete(true);
                            inn.setHomeRuns(1);
                            inn.setAwayRuns(0);
                            box.getBoxScore().put(Integer.toString(j), inn);
                        });
                        game.setBoxScore(box);
                    }

                    games.add(game);
                }
                else if (betterTeam.isPresent() && !worseTeam.isPresent()) {
                    Game game = new Game();
                    game.setHomeTeam(betterTeam.get());
                    game.setAwayTeam(betterTeam.get());

                    BoxScore box = game.getBoxScore();
                    IntStream.range(1,9).forEach(j->{
                        Inning inn = new Inning(i);
                        inn.setComplete(true);
                        inn.setHomeRuns(1);
                        inn.setAwayRuns(0);
                        box.getBoxScore().put(Integer.toString(j), inn);
                    });
                    game.setBoxScore(box);
                    games.add(game);

                }
                else if (!betterTeam.isPresent() && worseTeam.isPresent()) {
                    Game game = new Game();
                    game.setHomeTeam(worseTeam.get());
                    game.setAwayTeam(worseTeam.get());
                    BoxScore box = game.getBoxScore();
                    IntStream.range(1,9).forEach(j->{
                        Inning inn = new Inning(i);
                        inn.setComplete(true);
                        inn.setHomeRuns(1);
                        inn.setAwayRuns(0);
                        box.getBoxScore().put(Integer.toString(j), inn);
                    });
                    game.setBoxScore(box);
                    games.add(game);
                }

            });
        }
        Schedule schedule = new Schedule();
        schedule.setSchedule(games);

        return schedule;
    }
//    List<Team> getWinners(Schedule schedule)
//    {
//        schedule.getSchedule().stream().map(games->games.getBoxScore()).filter(box->box.getHomeScore());
//
//    }
}
