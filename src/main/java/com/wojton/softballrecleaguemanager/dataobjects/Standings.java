package com.wojton.softballrecleaguemanager.dataobjects;

import com.google.gson.GsonBuilder;
import com.wojton.softballrecleaguemanager.models.StandingsModel;
import com.wojton.softballrecleaguemanager.observers.BoxScoreObserver;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.wojton.softballrecleaguemanager.models.StandingsModel.loadStandings;

public class Standings implements BoxScoreObserver {
    private Map<String, WinsLoseTie> standingsMap = new HashMap<>();
    public Standings(Map<String, Team> teams)
    {
        setUpStandingsMap(teams);
    }
    private void setUpStandingsMap(Map<String, Team> teams)
    {
        standingsMap = getStandingsMap();
        if(standingsMap==null || standingsMap.size() == 0) {
            System.out.println("This is the beginning of standinds");
            teams.values().stream().forEach(team -> {
                standingsMap.put(team.getName(), new WinsLoseTie(team.getName()));
            });
        }
    }
    public Map<String, WinsLoseTie> getStandingsPlain() {
        return standingsMap;
    }

    public Map<String, WinsLoseTie> getStandingsMap() {
        Map<String, WinsLoseTie> loadedStandings;
        try {
            loadedStandings = loadStandings().getStandingsPlain();
        }
        catch(NullPointerException e)
        {
            loadedStandings = null;
        }
        if(loadedStandings != null && standingsMap.size() <= loadedStandings.size() && !loadedStandings.values().stream().allMatch(this::checkStanding))
        {
            standingsMap = loadedStandings;
        }
        return standingsMap;
    }
    private boolean checkStanding(WinsLoseTie wlt)
    {
        return wlt.getLoses() ==0 && wlt.getTies() == 0 && wlt.getWins() == 0 ? true : false;
    }

     public void setStandings(Schedule schedule) throws IOException {
        standingsMap.forEach((k,v) -> standingsMap.put(k,new WinsLoseTie(k)));
         schedule.getSchedule().stream().map(game->game.getBoxScore()).forEach(this::notify);
         StandingsModel.saveStandings(this);
     }
    @Override
    public void notify(BoxScore boxscore) {
       this.addBoxScore(boxscore);
    }
    protected void addBoxScore(BoxScore boxscore)
    {
        if(boxscore.getAwayTeam()!=null || boxscore.getHomeTeam()!=null) {
            if(standingsMap.containsKey(boxscore.getAwayTeam().getName()) && standingsMap.containsKey(boxscore.getHomeTeam().getName())) {


                if (boxscore.getBoxScore().values().stream().allMatch(score -> score.isComplete() == true)) {
                    boxscore.sumHits();
                    if (boxscore.getHomeScore() > boxscore.getAwayScore()) {
                        standingsMap.get(boxscore.getHomeTeam().getName()).addWin();
                        standingsMap.get(boxscore.getAwayTeam().getName()).addLoss();
                    } else if (boxscore.getHomeScore() < boxscore.getAwayScore()) {
                        standingsMap.get(boxscore.getHomeTeam().getName()).addLoss();
                        standingsMap.get(boxscore.getAwayTeam().getName()).addWin();
                    } else {
                        standingsMap.get(boxscore.getHomeTeam().getName()).addTies();
                        standingsMap.get(boxscore.getAwayTeam().getName()).addTies();
                    }
                }
            }
        }
    }
}
