package com.wojton.softballrecleaguemanager.controllers;

import com.google.gson.Gson;
import com.wojton.softballrecleaguemanager.SoftballRecLeagueManager;
import com.wojton.softballrecleaguemanager.dataobjects.Player;
import com.wojton.softballrecleaguemanager.dataobjects.Position;
import com.wojton.softballrecleaguemanager.dataobjects.Team;
import com.wojton.softballrecleaguemanager.models.TeamModel;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class AddPlayerController {
    @FXML
    Label playerName;
    @FXML
    Label position;
    @FXML
    Label number;
    @FXML
    TextField playerNameField;
    @FXML
    ComboBox<Position> positionField;
    @FXML
    TextField numberField;
    @FXML
    Button saveButton;
    @FXML
    Button cancelButton;

    private Team team;
    private TeamModel teamModel = new TeamModel();

    @FXML
    public void initialize()
    {
        this.playerName.setText("Name: ");
        this.number.setText("Number: ");
        this.position.setText("Position: ");
        this.saveButton.setText("Save Player");
        this.cancelButton.setText("Cancel");
        this.positionField.getItems().setAll(Position.values());


    }

    public void setTeam(Team team)
    {
        this.team = team;
    }

    public void onSaveButton() throws IOException {
        Player player = new Player();
        player.setName(playerNameField.getText());
        player.setNumber(Integer.parseInt(numberField.getText()));
        player.setPreferedPosition(positionField.getValue());
        team.getAvailablePlayers().add(player);
        teamModel.saveTeam(team);
        System.out.println(new Gson().toJson(player));
        Stage stage = (Stage) playerName.getScene().getWindow();
        FXMLLoader fxmlLoader = new FXMLLoader(SoftballRecLeagueManager.class.getResource("controllers/LeagueManager-View.fxml"));
        stage.getScene().setRoot(fxmlLoader.load());

    }
    public void onCancelButton() throws IOException {
        Stage stage = (Stage) playerName.getScene().getWindow();
        FXMLLoader fxmlLoader = new FXMLLoader(SoftballRecLeagueManager.class.getResource("controllers/LeagueManager-View.fxml"));
        stage.getScene().setRoot(fxmlLoader.load());
    }
}
