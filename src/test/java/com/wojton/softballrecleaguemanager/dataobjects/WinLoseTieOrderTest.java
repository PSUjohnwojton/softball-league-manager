package com.wojton.softballrecleaguemanager.dataobjects;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wojton.softballrecleaguemanager.models.RegularSeasonModel;
import com.wojton.softballrecleaguemanager.observers.BoxScoreObserver;
import com.wojton.softballrecleaguemanager.seasoncreators.PlayOffSeasonCreator;
import com.wojton.softballrecleaguemanager.seasoncreators.regularSeasonCreator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class WinLoseTieOrderTest {
    BoxScore boxScore;
    Schedule schedule;
    Standings standings;
    Map<String, Team> teamsMap = new HashMap<>();
    final private String userDataPath = System.getenv("LOCALAPPDATA") + "/data/userData";
    final private String userDateFile = userDataPath + "/" + "standings.txt";
    Standings setUp(int size) throws IOException {
        boxScore = null;
        schedule = null;
        standings =null;
        teamsMap.clear();
        ArrayList<Team> teams = new ArrayList<>();
        for(int i = 0; i<size; i++)
        {

            Team team = new Team();
            team.setName(Integer.toString(i));
            team.setOwner(Integer.toString(i));
            team.setMascot(Integer.toString(i));
            teams.add(team);
            teamsMap.put(Integer.toString(i), team);

        }
        standings = new Standings(teamsMap);
        regularSeasonCreator creator = new regularSeasonCreator();
        RegularSeasonModel.saveSchedule(creator.createSchedule(teams));
        schedule = RegularSeasonModel.getSchedule(teams);
        schedule.getSchedule().stream().forEach(game -> {
             boxScore = new BoxScore(game.getAwayTeam(), game.getHomeTeam());
            HashMap<String, Inning> innings = new HashMap<>();
            if(game.getHomeTeam().getName().equals("1"))
            {
                IntStream.range(1,10).forEach(i ->
                {
                    Inning inn = new Inning(i);
                    inn.setAwayRuns(0);
                    inn.setHomeRuns(1);
                    inn.setComplete(true);
                    innings.put(Integer.toString(i),inn);
                });
            }
            else if (game.getAwayTeam().getName().equals("1"))
            {
                IntStream.range(1,10).forEach(i ->
                {
                    Inning inn = new Inning(i);
                    inn.setAwayRuns(1);
                    inn.setHomeRuns(0);
                    inn.setComplete(true);
                    innings.put(Integer.toString(i),inn);
                });
            }
            else if (game.getHomeTeam().getName().equals("2"))
            {
                IntStream.range(1,10).forEach(i ->
                {
                    Inning inn = new Inning(i);
                    inn.setAwayRuns(1);
                    inn.setHomeRuns(0);
                    inn.setComplete(true);
                    innings.put(Integer.toString(i),inn);
                });
            }
            else if (game.getAwayTeam().getName().equals("2"))
            {
                IntStream.range(1,10).forEach(i ->
                {
                    Inning inn = new Inning(i);
                    inn.setAwayRuns(0);
                    inn.setHomeRuns(1);
                    inn.setComplete(true);
                    innings.put(Integer.toString(i),inn);
                });
            }
            else
            {
                IntStream.range(1,10).forEach(i ->
                {
                    Inning inn = new Inning(i);
                    inn.setAwayRuns(0);
                    inn.setHomeRuns(1);
                    inn.setComplete(true);
                    innings.put(Integer.toString(i),inn);
                });
            }
            boxScore.setBoxScore(innings);
            standings.addBoxScore(boxScore);
        });


    return standings;
    }

    @Test
    void orderStandings() throws IOException {
      standings =   this.setUp(4);
        WinLoseTieOrder order = new WinLoseTieOrder();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        ArrayList<WinsLoseTie> standings1 = (ArrayList<WinsLoseTie>) order.orderStandings(standings);
        Assertions.assertTrue(standings1.get(0).getTeamName().equals("1"));
        Assertions.assertTrue(standings1.get(3).getTeamName().equals("2"));

    }
    @Test
    void order8Standings() throws IOException {
        standings =    this.setUp(8);
        WinLoseTieOrder order = new WinLoseTieOrder();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        ArrayList<WinsLoseTie> standings1 = (ArrayList<WinsLoseTie>) order.orderStandings(standings);
        Assertions.assertTrue(standings1.get(0).getTeamName().equals("1"));
        Assertions.assertTrue(standings1.get(7).getTeamName().equals("2"));

    }
    @Test
    void order12Standings() throws IOException {
        standings =    this.setUp(12);
        WinLoseTieOrder order = new WinLoseTieOrder();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        ArrayList<WinsLoseTie> standings1 = (ArrayList<WinsLoseTie>) order.orderStandings(standings);
        Assertions.assertTrue(standings1.get(0).getTeamName().equals("1"));
        Assertions.assertTrue(standings1.get(11).getTeamName().equals("2"));

    }
    @Test
    void testPlayoffSchedule() throws IOException {
        standings =     this.setUp(4);
        WinLoseTieOrder order = new WinLoseTieOrder();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        System.out.println( gson.toJson(order.orderStandings(standings)));
        ArrayList<Team> teams = new ArrayList<>(teamsMap.values());
        PlayOffSeasonCreator creator = new PlayOffSeasonCreator(standings);
        Schedule schedule = creator.createSchedule(teams);
        Assertions.assertTrue(schedule.getSchedule().get(0).getHomeTeam().getName().equals("1"));
        Assertions.assertTrue(schedule.getSchedule().get(0).getAwayTeam().getName().equals("2"));

        System.out.println("\n\n\n this is the name: " +schedule.getSchedule().get(1).getHomeTeam().getName());
        Assertions.assertTrue(schedule.getSchedule().get(1).getHomeTeam().getName().equals("0")
                ||schedule.getSchedule().get(1).getAwayTeam().getName().equals("0") );
        Assertions.assertTrue(schedule.getSchedule().get(1).getAwayTeam().getName().equals("3")
        || schedule.getSchedule().get(1).getHomeTeam().getName().equals("3"));




    }

    @Test
    void test8PlayoffSchedule() throws IOException {
        standings =     this.setUp(8);
        WinLoseTieOrder order = new WinLoseTieOrder();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        System.out.println( gson.toJson(order.orderStandings(standings)));
        ArrayList<Team> teams = new ArrayList<>(teamsMap.values());
        PlayOffSeasonCreator creator = new PlayOffSeasonCreator(standings);
        Schedule schedule = creator.createSchedule(teams);
        Assertions.assertTrue(schedule.getSchedule().get(0).getHomeTeam().getName().equals("1"));
        Assertions.assertTrue(schedule.getSchedule().get(0).getAwayTeam().getName().equals("2"));




    }

    @Test
    void test12PlayoffSchedule() throws IOException {
        standings =     this.setUp(12);
        WinLoseTieOrder order = new WinLoseTieOrder();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        System.out.println( gson.toJson(order.orderStandings(standings)));
        ArrayList<Team> teams = new ArrayList<>(teamsMap.values());
        PlayOffSeasonCreator creator = new PlayOffSeasonCreator(standings);
        Schedule schedule = creator.createSchedule(teams);
        Assertions.assertTrue(schedule.getSchedule().get(0).getHomeTeam().getName().equals("1"));
        Assertions.assertTrue(schedule.getSchedule().get(0).getAwayTeam().getName().equals("2"));




    }

}