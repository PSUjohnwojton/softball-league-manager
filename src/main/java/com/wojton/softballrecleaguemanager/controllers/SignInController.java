package com.wojton.softballrecleaguemanager.controllers;

import com.wojton.softballrecleaguemanager.SoftballRecLeagueManager;
import com.wojton.softballrecleaguemanager.models.CredentialsModel;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class SignInController {

    @FXML
    Label userName;
    @FXML
    Label password;
    @FXML
    TextField userNameTextField;
    @FXML
    TextField passwordTextField;
    @FXML
    Button signIn;
    @FXML
    Button cancel;
    @FXML
    Button back;
    @FXML
    Label error;
    private CredentialsModel credentialsModel;
    @FXML
    public void initialize() {
        userName.setText("User Name: ");
        password.setText("Password: ");
        signIn.setText("Confirm");
        cancel.setText("Cancel");
        back.setText("Back");
        credentialsModel = new CredentialsModel();
    }

    @FXML
    protected void onSignInClick() throws IOException {
        if (credentialsModel.loadCredentials(userNameTextField.getText(), passwordTextField.getText())) {
            loadNextScreen();
        } else {
            error.setText("Error logging in... Try again.");
        }

    }


    @FXML
    protected void onCancelClick() throws IOException {
        onBackClick();
    }
    @FXML
    protected void onBackClick() throws IOException{
        Stage stage = (Stage) userName.getScene().getWindow();
        // these two of them return the same stage
        // Swap screen
        FXMLLoader fxmlLoader = new FXMLLoader(SoftballRecLeagueManager.class.getResource("StartScreen-view.fxml"));
        stage.getScene().setRoot(fxmlLoader.load());
    }
    private void loadNextScreen() throws IOException
    {
        Stage stage = (Stage) userName.getScene().getWindow();
        // these two of them return the same stage
        // Swap screen
        FXMLLoader fxmlLoader = new FXMLLoader(SoftballRecLeagueManager.class.getResource("controllers/LeagueManager-View.fxml"));
        stage.getScene().setRoot(fxmlLoader.load());
    }
}
